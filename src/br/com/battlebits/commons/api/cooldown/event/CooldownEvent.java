package br.com.battlebits.commons.api.cooldown.event;

import br.com.battlebits.commons.api.cooldown.types.Cooldown;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

/**
 * Arquivo criado em 02/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
@RequiredArgsConstructor
public abstract class CooldownEvent extends Event {

    @Getter
    @NonNull
    private Player player;

    @Getter
    @NonNull
    private Cooldown cooldown;
}

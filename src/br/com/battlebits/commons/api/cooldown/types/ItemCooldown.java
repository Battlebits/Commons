package br.com.battlebits.commons.api.cooldown.types;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.inventory.ItemStack;

/**
 * Arquivo criado em 02/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class ItemCooldown extends Cooldown {

    @Getter
    private ItemStack item;

    @Getter
    @Setter
    private boolean selected;

    public ItemCooldown(ItemStack item, String name, Long duration) {
        super(name, duration);
        this.item = item;
    }
}

package br.com.battlebits.commons.bungee.party;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.data.DataParty;
import br.com.battlebits.commons.core.party.Party;
import br.com.battlebits.commons.core.translate.T;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

public class BungeeParty extends Party {

    protected transient ScheduledTask task;

    @Getter
    protected Map<UUID, Long> queue = new HashMap<>();
    protected Map<UUID, ScheduledTask> logout = new HashMap<>();

    public BungeeParty(UUID owner) {
        super(owner);
    }

    @Override
    public void init() {
        queue = new HashMap<>();
        logout = new HashMap<>();
    }

    @Override
    public void onOwnerJoin() {
        if (task != null) {
            task.cancel();
            task = null;
        }
    }

    @Override
    public void onOwnerLeave() {
        task = ProxyServer.getInstance().getScheduler().schedule(BungeeMain.getPlugin(), new Runnable() {
            @Override
            public void run() {
                DataParty.unloadParty(BungeeParty.this);
                BattlebitsAPI.getPartyCommon().removeParty(getOwner());
                logout.values().forEach(id -> ProxyServer.getInstance().getScheduler().cancel(id));
            }
        }, 3L, TimeUnit.MINUTES);
    }

    @Override
    public void onMemberJoin(UUID member) {
        if (logout.containsKey(member)) {
            logout.remove(member).cancel();
        }
    }

    @Override
    public void onMemberLeave(UUID member) {
        logout.put(member, ProxyServer.getInstance().getScheduler().schedule(BungeeMain.getPlugin(), () -> removeMember(member), 1L, TimeUnit.MINUTES));
    }

    public void addInvite(UUID uuid) {
        queue.put(uuid, System.currentTimeMillis() + (60 * 1000));
    }

    @Override
    public void sendMessage(boolean prefix, boolean translate, String id, String[]... replacement) {
        for (UUID uuid : Stream.concat(Stream.of(getOwner()), getMembers().stream()).toArray(UUID[]::new)) {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
            if (player != null) {
                player.sendMessage(TextComponent.fromLegacyText((prefix ? PREFIX : "") + (translate ? T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(uuid), id, replacement) : id)));
            }
        }
    }

    @Override
    public int getOnlineCount() {
        int count = 0;

        if (ProxyServer.getInstance().getPlayer(getOwner()) != null)
            count++;

        for (UUID uuid : getMembers())
            if (ProxyServer.getInstance().getPlayer(uuid) != null)
                count++;

        return count;
    }

    @Override
    public void removeMember(UUID member) {
        if (logout.containsKey(member))
            logout.remove(member);
        super.removeMember(member);
    }

}

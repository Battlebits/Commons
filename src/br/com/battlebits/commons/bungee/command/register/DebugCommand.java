package br.com.battlebits.commons.bungee.command.register;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.bungee.command.BungeeCommandArgs;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.server.loadbalancer.server.BattleServer;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.util.conversor.Conversor;
import br.com.battlebits.commons.util.conversor.DoubleXPAndMoney;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class DebugCommand implements CommandClass {

    @CommandFramework.Command(name = "convert", usage = "/<command>", groupToUse = Group.DONO)
    public void connect(BungeeCommandArgs cmdArgs) {
        try {
            Conversor.convert();
            System.out.println("COntas convertidas com sucesso");
        } catch (Exception e) {
            System.out.println("Erro ao converter contas");
            e.printStackTrace();
        }

    }

    @CommandFramework.Command(name = "generateMoney", usage = "/<command>", groupToUse = Group.DONO)
    public void generateMoney(BungeeCommandArgs cmdArgs) {
        try {
            DoubleXPAndMoney.convert();
            System.out.println("Money salvo com sucesso");
        } catch (Exception e) {
            System.out.println("Erro ao converter contas");
            e.printStackTrace();
        }

    }

}

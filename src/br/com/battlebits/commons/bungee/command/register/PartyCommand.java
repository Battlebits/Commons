package br.com.battlebits.commons.bungee.command.register;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.bungee.command.BungeeCommandArgs;
import br.com.battlebits.commons.bungee.party.BungeeParty;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.commons.core.command.CommandFramework.Completer;
import br.com.battlebits.commons.core.data.DataParty;
import br.com.battlebits.commons.core.party.Party;
import br.com.battlebits.commons.core.translate.T;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PartyCommand implements CommandClass {

    private final String[] subCommands = new String[]{"accept", "invite", "remove", "leave", "list"};

    @Command(name = "party", usage = "</command>", aliases = "p", runAsync = true)
    public void onCommand(BungeeCommandArgs cmdArgs) {
        if (cmdArgs.isPlayer()) {
            String[] args = cmdArgs.getArgs();
            ProxiedPlayer player = cmdArgs.getPlayer();

            if (args.length == 2 && args[0].equalsIgnoreCase("accept")) {
                BungeeParty party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                if (party == null) {
                    ProxiedPlayer target = BattlebitsAPI.getPlatform().getPlayerExact(args[1], ProxiedPlayer.class);
                    if (target != null) {
                        party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(target.getUniqueId());
                        if (party != null && party.isOwner(target.getUniqueId())) {
                            if (party.getQueue().containsKey(player.getUniqueId())) {
                                long time = party.getQueue().remove(player.getUniqueId());
                                if (time > System.currentTimeMillis()) {
                                    party.addMember(player.getUniqueId());
                                    party.sendMessage("command-party-new-member", new String[]{"%member%", player.getName()});
                                    DataParty.saveRedisParty(party);
                                    DataParty.saveRedisPartyField(party, "members");
                                } else {
                                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-expired")));
                                }
                            } else {
                                player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-not-invited")));
                            }
                        } else {
                            player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-not-invited")));
                        }
                    } else {
                        player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-player-offline", new String[]{"%player%", args[1]})));
                    }
                } else {
                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-already-in-party")));
                }

                return;
            }

            if (args.length == 2 && args[0].equalsIgnoreCase("invite")) {
                BungeeParty party = (BungeeParty) BattlebitsAPI.getPartyCommon().getByOwner(player.getUniqueId());
                if (party == null) {
                    party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                    if (party == null) {
                        party = new BungeeParty(player.getUniqueId());
                        BattlebitsAPI.getPartyCommon().loadParty(party);
                        DataParty.saveRedisParty(party);
                        DataParty.loadParty(party);
                    } else if (!party.isOwner(player.getUniqueId())) {
                        player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-owner-only")));
                        return;
                    }
                }

                if (party != null) {
                    ProxiedPlayer target = BattlebitsAPI.getPlatform().getPlayerExact(args[1], ProxiedPlayer.class);
                    if (target != null) {
                        if (!target.getUniqueId().equals(player.getUniqueId())) {
                            long time = party.getQueue().getOrDefault(target.getUniqueId(), 0L);
                            if (time < System.currentTimeMillis()) {
                                party.addInvite(target.getUniqueId());

                                player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-invite-player", new String[]{"%player%", target.getName()})));
                                target.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-invite-owner", new String[]{"%owner%", player.getName()})));

                                TextComponent clickHere = new TextComponent(T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "here"));
                                clickHere.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party accept " + player.getName()));
                                clickHere.setColor(ChatColor.YELLOW);
                                clickHere.setUnderlined(true);
                                clickHere.setBold(true);

                                TextComponent extraText = new TextComponent(TextComponent.fromLegacyText(" " + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-invite-extra", new String[]{"%owner%", player.getName()})));
                                TextComponent textComponent = new TextComponent(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "click") + " "));
                                textComponent.addExtra(clickHere);
                                textComponent.addExtra(extraText);
                                target.sendMessage(textComponent);
                            } else {
                                player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-already-invited", new String[]{"%player%", target.getName()})));
                            }
                        } else {
                            player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-not-invite-yourself")));
                        }
                    } else {
                        player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-player-offline", new String[]{"%player%", args[1]})));
                    }
                }

                return;
            }

            if (args.length == 2 && args[0].equalsIgnoreCase("remove")) {
                BungeeParty party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                if (party != null) {
                    if (party.isOwner(player.getUniqueId())) {
                        ProxiedPlayer target = BattlebitsAPI.getPlatform().getPlayerExact(args[1], ProxiedPlayer.class);
                        if (target != null) {
                            if (!player.getUniqueId().equals(target.getUniqueId())) {
                                if (party.getMembers().contains(target.getUniqueId())) {
                                    party.getMembers().remove(target.getUniqueId());
                                    DataParty.saveRedisPartyField(party, "members");
                                    DataParty.saveRedisParty(party);

                                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-remove-player-to-owner", new String[]{"%member%", target.getName()})));
                                    target.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-remove-player-to-member", new String[]{"%owner%", player.getName()})));
                                } else {
                                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-player-is-not-in-party", new String[]{"%player%", target.getName()})));
                                }
                            } else {
                                player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-not-remove-yourself")));
                            }
                        } else {
                            player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-player-offline", new String[]{"%player%", args[1]})));
                        }
                    } else {
                        player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-owner-only")));
                    }
                } else {
                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-is-not-in-party")));
                }

                return;
            }

            if (args.length == 1 && args[0].equalsIgnoreCase("leave")) {
                BungeeParty party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                if (party != null) {
                    if (!party.isOwner(player.getUniqueId())) {
                        party.sendMessage("command-party-member-leave", new String[]{"%member%", player.getName()});
                        party.getMembers().remove(player.getUniqueId());
                        DataParty.saveRedisPartyField(party, "members");
                        DataParty.saveRedisParty(party);
                    } else {
                        party.sendMessage("command-party-owner-leave", new String[]{"%owner%", player.getName()});
                        BattlebitsAPI.getPartyCommon().removeParty(party);
                        DataParty.unloadParty(party);
                        DataParty.disbandParty(party);
                    }
                } else {
                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-is-not-in-party")));
                }

                return;
            }

            if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
                BungeeParty party = (BungeeParty) BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                if (party != null) {
                    String list = Stream.concat(Stream.of(party.getOwner()), party.getMembers().stream()).map(u -> BattlebitsAPI.getNameOf(u)).sorted((s1, s2) -> s1.compareToIgnoreCase(s2)).collect(Collectors.joining("§f,§e "));
                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-player-list", new String[]{"%list%", list})));
                } else {
                    player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-is-not-in-party")));
                }

                return;
            }

            String commands = T.t(BungeeMain.getPlugin(), cmdArgs.getLanguage(), "command-party-commands");
            for (String command : Stream.of(commands.split("\n")).map(s -> Party.PREFIX + s).toArray(String[]::new)) {
                player.sendMessage(TextComponent.fromLegacyText(command));
            }
        }
    }

    @Completer(name = "party", aliases = {"p"})
    public List<String> onCompleter(BungeeCommandArgs commandArgs) {
        List<String> list = new ArrayList<>();
        if (commandArgs.isPlayer()) {
            String[] args = commandArgs.getArgs();
            if (args.length > 0) {
                ProxiedPlayer player = commandArgs.getPlayer();
                Party party = BattlebitsAPI.getPartyCommon().getParty(player.getUniqueId());
                if (party != null && party.isOwner(player.getUniqueId())) {
                    if (args.length < 3 && args[0].equalsIgnoreCase("invite")) {
                        for (ProxiedPlayer target : ProxyServer.getInstance().getPlayers()) {
                            if (!target.equals(player) && (args.length < 2 || find(target, args[1]))) {
                                if (BattlebitsAPI.getPartyCommon().inParty(target.getUniqueId()))
                                    continue;
                                list.add(target.getName());
                            }
                        }
                    } else if (args.length < 3 && args[0].equalsIgnoreCase("remove")) {
                        for (UUID uuid : party.getMembers()) {
                            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(uuid);
                            if (target != null && !target.equals(player) && (args.length < 2 || find(target, args[1]))) {
                                list.add(target.getName());
                            }
                        }
                    } else if (args.length == 1) {
                        for (String sub : subCommands) {
                            if (find(sub, args[0])) {
                                list.add(sub);
                            }
                        }
                    }
                } else if (party == null) {
                    if (args.length < 3 && args[0].equalsIgnoreCase("invite")) {
                        for (ProxiedPlayer target : ProxyServer.getInstance().getPlayers()) {
                            if (!target.equals(player) && (args.length < 2 || find(target, args[1]))) {
                                if (BattlebitsAPI.getPartyCommon().inParty(target.getUniqueId()))
                                    continue;
                                list.add(target.getName());
                            }
                        }
                    } else if (args.length < 3 && args[0].equalsIgnoreCase("accept")) {
                        for (Party p : BattlebitsAPI.getPartyCommon().getPartys()) {
                            if (p instanceof BungeeParty) {
                                long time = ((BungeeParty) p).getQueue().getOrDefault(player.getUniqueId(), 0L);
                                if (time > System.currentTimeMillis()) {
                                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getOwner());
                                    if (target != null && !target.equals(player) && (args.length < 2 || find(target, args[1]))) {
                                        list.add(target.getName());
                                    }
                                }
                            }
                        }
                    } else if (args.length == 1) {
                        for (String sub : subCommands) {
                            if (find(sub, args[0])) {
                                list.add(sub);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    private boolean find(ProxiedPlayer player, String name) {
        return find(player.getName(), name);
    }

    private boolean find(String s1, String s2) {
        return s1.toLowerCase().startsWith(s2.toLowerCase());
    }
}

package br.com.battlebits.commons.bungee.listener;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.account.League;
import br.com.battlebits.commons.core.data.DataPlayer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.server.ServerManager;
import br.com.battlebits.commons.core.server.ServerType;
import br.com.battlebits.commons.core.server.loadbalancer.server.BattleServer;
import br.com.battlebits.commons.core.translate.T;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.List;
import java.util.UUID;

public class MessageListener implements Listener {
    private ServerManager manager;

    public MessageListener(ServerManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onPluginMessageBungeeCordChannel(PluginMessageEvent event) {
        if (!event.getTag().equals("BungeeCord"))
            return;
        if (!(event.getSender() instanceof Server))
            return;
        if (!(event.getReceiver() instanceof ProxiedPlayer))
            return;
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) event.getReceiver();
        BattlePlayer player = BattlebitsAPI.getAccountCommon().getBattlePlayer(proxiedPlayer.getUniqueId());
        ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
        String subChannel = in.readUTF();
        switch (subChannel) {
            case "ChallengerUpdate": {
                event.setCancelled(true);
                List<UUID> uuidList = DataPlayer.getChallengerList();
                if(uuidList.size() <= 100)
                    break;
                for(int i = 100; i < uuidList.size(); i++) {
                    BattlePlayer battlePlayer = BattlebitsAPI.getAccountCommon().getBattlePlayer(uuidList.get(i));
                    if (battlePlayer == null) {
                        try {
                            player = DataPlayer.getPlayer(uuidList.get(i));
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                        if (player == null) {
                            break;
                        }
                    }
                    battlePlayer.setLeague(League.LEGENDARY);
                }
                break;
            }

            case "Hungergames": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.HUNGERGAMES).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "SWNS": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.SWNS).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "SWNT": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.SWNT).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "SWIS": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.SWIS).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "SWIT": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.SWIT).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "CustomHungergames": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.CUSTOMHG).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }
            case "DoubleKitHungergames": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.DOUBLEKITHG).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }
            case "Fairplayhg": {
                event.setCancelled(true);
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "PVPFulliron": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.PVP_FULLIRON).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }

            case "PVPSimulator": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.PVP_SIMULATOR).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }
            case "SWLobby": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.SKYWARS_LOBBY).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }
            case "Lobby": {
                event.setCancelled(true);
                BattleServer server = manager.getBalancer(ServerType.LOBBY).next();
                if (server != null && server.getServerInfo() != null) {
                    if (!server.isFull() || (server.isFull() && player.hasGroupPermission(Group.ULTIMATE))) {
                        proxiedPlayer.connect(server.getServerInfo());
                        break;
                    }
                }
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(proxiedPlayer.getUniqueId()), "server-not-available")));
                break;
            }
            default:
                break;
        }
    }

    @EventHandler
    public void onAnticheatMessage(PluginMessageEvent event) {
        if (!event.getTag().equals(BattlebitsAPI.getBungeeChannel()))
            return;
        if (!(event.getSender() instanceof Server))
            return;
        if (!(event.getReceiver() instanceof ProxiedPlayer))
            return;
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) event.getReceiver();
        BattlePlayer player = BattlebitsAPI.getAccountCommon().getBattlePlayer(proxiedPlayer.getUniqueId());
        if (player == null)
            return;
        ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
        String subChannel = in.readUTF();
        switch (subChannel) {
            case "AnticheatAlert": {
                event.setCancelled(true);
                double tps = in.readDouble();
                int ping = in.readInt();
                String suspect = in.readUTF();
                for (ProxiedPlayer online : BungeeMain.getPlugin().getProxy().getPlayers()) {
                    BattlePlayer pl = BattlebitsAPI.getAccountCommon().getBattlePlayer(online.getUniqueId());
                    if (pl.hasGroupPermission(Group.TRIAL) && pl.getConfiguration().isAlertsEnabled()) {
                        TextComponent message = new TextComponent(T.t(BungeeMain.getPlugin(), pl.getLanguage(),
                                "anticheat-alert-prefix")
                                + " "
                                + T.t(BungeeMain.getPlugin(), pl.getLanguage(), "anticheat-alert-player",
                                new String[]{"%player%", "%tps%", "%ping%", "%suspect%"},
                                new String[]{player.getName(), "" + tps, "" + ping, suspect}));
                        message.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/teleport " + player.getName()));
                        message.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT,
                                new TextComponent[]{new TextComponent(
                                        T.t(BungeeMain.getPlugin(), pl.getLanguage(), "click-to-teleport"))}));
                        online.sendMessage(message);
                    }
                }
                break;
            }
            case "AnticheatBanAlert": {
                event.setCancelled(true);
                String reason = in.readUTF();
                for (ProxiedPlayer online : BungeeMain.getPlugin().getProxy().getPlayers()) {
                    BattlePlayer pl = BattlebitsAPI.getAccountCommon().getBattlePlayer(online.getUniqueId());
                    if (pl != null && pl.hasGroupPermission(Group.TRIAL) && pl.getConfiguration().isAlertsEnabled()) {
                        TextComponent message = new TextComponent(
                                T.t(BungeeMain.getPlugin(), pl.getLanguage(), "anticheat-alert-prefix") + " "
                                        + T.t(BungeeMain.getPlugin(), pl.getLanguage(), "anticheat-alert-ban",
                                        new String[]{"%player%", "%reason%"},
                                        new String[]{player.getName(), reason}));
                        message.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/teleport " + player.getName()));
                        message.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT,
                                new TextComponent[]{new TextComponent(
                                        T.t(BungeeMain.getPlugin(), pl.getLanguage(), "click-to-teleport"))}));
                        online.sendMessage(message);
                    }
                }
                break;
            }
            case "AnticheatKickAlert": {
                event.setCancelled(true);
                String reason = in.readUTF();
                for (ProxiedPlayer online : BungeeMain.getPlugin().getProxy().getPlayers()) {
                    BattlePlayer pl = BattlebitsAPI.getAccountCommon().getBattlePlayer(online.getUniqueId());
                    if (pl.hasGroupPermission(Group.TRIAL) && pl.getConfiguration().isAlertsEnabled()) {
                        TextComponent message = new TextComponent(
                                T.t(BungeeMain.getPlugin(), pl.getLanguage(), "anticheat-alert-prefix") + " "
                                        + T.t(BungeeMain.getPlugin(), pl.getLanguage(), "anticheat-alert-kick",
                                        new String[]{"%player%", "%reason%"},
                                        new String[]{player.getName(), reason}));
                        message.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/teleport " + player.getName()));
                        message.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT,
                                new TextComponent[]{new TextComponent(
                                        T.t(BungeeMain.getPlugin(), pl.getLanguage(), "click-to-teleport"))}));
                        online.sendMessage(message);
                    }
                }
                break;
            }
            case "AnticheatBan": {
                event.setCancelled(true);
                String banReason = in.readUTF();
                BungeeMain.getPlugin().getProxy().getPluginManager().dispatchCommand(
                        BungeeMain.getPlugin().getProxy().getConsole(),
                        "tempban " + player.getUniqueId().toString() + " 2h " + banReason);
                break;
            }
            default:
                break;
        }
    }
}

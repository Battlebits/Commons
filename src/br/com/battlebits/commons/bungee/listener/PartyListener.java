package br.com.battlebits.commons.bungee.listener;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.party.Party;
import br.com.battlebits.commons.core.server.ServerType;
import br.com.battlebits.commons.core.server.loadbalancer.server.BattleServer;
import br.com.battlebits.commons.core.translate.T;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PartyListener implements Listener {

    private final Set<UUID> allow = new HashSet<>();

    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        BattleServer server = BungeeMain.getPlugin().getServerManager().getServer(event.getTarget().getName());
        if (player.getServer() != null && server != null && !server.getServerType().isLobby() && server.getServerType() != ServerType.NONE) {
            Party party = BattlebitsAPI.getPartyCommon().getByOwner(player.getUniqueId());
            if (party != null) {
                for (UUID uuid : party.getMembers()) {
                    ProxiedPlayer member = ProxyServer.getInstance().getPlayer(uuid);
                    if (member == null) {
                        player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(player.getUniqueId()), "command-party-member-offline", new String[]{"%member%", BattlebitsAPI.getNameOf(uuid)})));
                        event.setCancelled(true);
                    } else if (member != null && member.getServer() != null && !member.getServer().getInfo().equals(event.getTarget())) {
                        allow.add(member.getUniqueId());
                        member.connect(event.getTarget());
                    }
                }
            } else if (allow.contains(player.getUniqueId())) {
                allow.remove(player.getUniqueId());
            } else if (BattlebitsAPI.getPartyCommon().inParty(player.getUniqueId())) {
                player.sendMessage(TextComponent.fromLegacyText(Party.PREFIX + T.t(BungeeMain.getPlugin(), BattlePlayer.getLanguage(player.getUniqueId()), "command-party-you-can-not-connect-minigame")));
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        allow.remove(event.getPlayer().getUniqueId());
    }

}

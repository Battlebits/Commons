package br.com.battlebits.commons.bungee.listener;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bungee.BungeeMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.sql.PreparedStatement;

/**
 * Created by gustavo on 07/06/17.
 */
public class LogListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(ChatEvent event) {
        if (!(event.getSender() instanceof ProxiedPlayer))
            return;

        BungeeMain.getPlugin().getProxy().getScheduler().runAsync(BungeeMain.getPlugin(), ()->{
            BattlePlayer player = BattlePlayer.getPlayer(((ProxiedPlayer)event.getSender()).getUniqueId());
            if(player == null)
                return;
            int protocolNumber = -1;
            if(ProxyServer.getInstance().getPlayer(player.getUniqueId()) != null)
                protocolNumber = ProxyServer.getInstance().getPlayer(player.getUniqueId()).getPendingConnection().getVersion();
            try (PreparedStatement stmt = BattlebitsAPI.getCommonsMysql().prepareStatment("INSERT INTO `player_log`(`uuid`, `protocol`, `event`, `server`, `servertype`, `date`, `args`) VALUES (?, " + protocolNumber + ", ?, ?, ?, NOW(), ?);")) {
                stmt.setString(1, player.getUniqueId().toString().replace("-",""));
                stmt.setString(2, "CHAT");
                stmt.setString(3, player.getServerConnected());
                stmt.setString(4, player.getServerConnectedType().toString());
                stmt.setString(5,event.getMessage());
                stmt.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


}

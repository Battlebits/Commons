package br.com.battlebits.commons.bungee;

import br.com.battlebits.commons.core.BattlePlatform;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

/**
 * Arquivo criado em 01/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class BungeePlatform implements BattlePlatform {

    @Override
    public UUID getUUID(String name) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
        return player != null ? player.getUniqueId() : null;
    }

    @Override
    public String getName(UUID uuid) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
        return player != null ? player.getName() : null;
    }

    @Override
    public boolean isOnline(UUID uuid) {
        return ProxyServer.getInstance().getPlayer(uuid) != null;
    }

    @Override
    public boolean isOnline(String name) {
        return getPlayerExact(name) != null;
    }

    @Override
    public <T> T getPlayerExact(String name, Class<T> clazz) {
        ProxiedPlayer found = null;
        if (name != null && !name.isEmpty()) {
            found = ProxyServer.getInstance().getPlayer(name);
            if (found == null) {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (player.getName().equalsIgnoreCase(name)) {
                        found = player;
                        break;
                    }
                }
            }
        }
        return found != null ? clazz.cast(found) : null;
    }

    @Override
    public void runAsync(Runnable runnable) {
        ProxyServer.getInstance().getScheduler().runAsync(BungeeMain.getPlugin(), runnable);
    }

}

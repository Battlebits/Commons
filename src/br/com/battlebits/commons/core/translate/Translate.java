package br.com.battlebits.commons.core.translate;

import java.util.HashMap;
import java.util.Map;

import br.com.battlebits.commons.core.data.DataServer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.backend.mongodb.MongoBackend;
import net.md_5.bungee.api.ChatColor;

@RequiredArgsConstructor
public class Translate {

    @NonNull
    private String database;

    @NonNull
    private MongoBackend backend;

    private Map<Language, Map<String, String>> languageTranslations = new HashMap<>();

    public String getTranslation(Language language, String id) {
        return getTranslation(language, id, null, null);
    }

    public String getTranslation(Language language, String id, Map<String, String> replacement) {
        String[] target = replacement.keySet().stream().toArray(String[]::new);
        String[] replace = replacement.values().stream().toArray(String[]::new);
        return getTranslation(language, id, target, replace);
    }

    public String getTranslation(Language language, String id, String[]... replacement) {
        String[] target = new String[replacement.length];
        String[] replace = new String[replacement.length];

        for (int i = 0; i < replacement.length; i++) {
            String[] s = replacement[i];
            if (s.length >= 2) {
                target[i] = s[0];
                replace[i] = s[1];
            }
        }

        return getTranslation(language, id, target, replace);
    }

    public String getTranslation(Language language, String id, String[] target, String[] replacement) {
        Map<String, String> map = languageTranslations.computeIfAbsent(language, v -> new HashMap<>());
        String message = ChatColor.translateAlternateColorCodes('&', map.computeIfAbsent(id, v -> addTag(language, id)));
        if (target != null && replacement != null) {
            for (int i = 0; i < Math.min(target.length, replacement.length); i++) {
                if (target[i] != null && replacement[i] != null) {
                    message = message.replace(target[i], replacement[i]);
                }
            }
        }
        return message;
    }

    public boolean contains(Language language, String id) {
        return languageTranslations.computeIfAbsent(language, v -> new HashMap<>()).containsKey(id);
    }

    public void loadTranslations() {
        for (Language lang : Language.values()) {
            languageTranslations.put(lang, loadTranslation(lang));
            BattlebitsAPI.debug(this.database + " > " + lang.toString() + " > CARREGADA");
        }
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> loadTranslation(Language language) {
        MongoDatabase database = backend.getClient().getDatabase(this.database);
        MongoCollection<Document> collection = database.getCollection("translation");
        Document found = collection.find(Filters.eq("language", language.toString())).first();
        if (found != null) {
            return (Map<String, String>) found.get("map");
        }
        collection.insertOne(new Document("language", language.toString()).append("map", new HashMap<>()));
        return new HashMap<>();
    }

    private String addTag(Language language, String id) {
        BattlebitsAPI.debug(language.toString() + " > " + id + " > NAO ENCONTRADA");
        BattlebitsAPI.getPlatform().runAsync(() -> DataServer.addTranslationTag(language, id, backend, database));
        return "[NOT FOUND: '" + id + "']";
    }

}

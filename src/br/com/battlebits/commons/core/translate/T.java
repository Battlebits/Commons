package br.com.battlebits.commons.core.translate;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class T {

    @Getter
    private static final Pattern pattern = Pattern.compile("(§%([\\S^)]+)%§)");
    private static final Map<Object, Translate> translates = new HashMap<>();

    public static String getTranslation(Language lang, String id) {
        String string = "[NOT FOUND: '" + id + "']";
        for (Translate t : translates.values()) {
            if (t.contains(lang, id)) {
                string = t.getTranslation(lang, id);
                break;
            }
        }
        return string;
    }

    public static void loadTranslate(Object main, Translate translate) {
        translates.put(main, translate);
    }

    public static String t(Object main, Language lang, String id) {
        return translates.containsKey(main) ? translates.get(main).getTranslation(lang, id) : "";
    }

    public static String t(Object main, Language lang, String id, Map<String, String> replace) {
        return translates.containsKey(main) ? translates.get(main).getTranslation(lang, id, replace) : "";
    }

    public static String t(Object main, Language lang, String id, String[]... replace) {
        return translates.containsKey(main) ? translates.get(main).getTranslation(lang, id, replace) : "";
    }

    public static String t(Object main, Language lang, String id, String[] target, String[] replace) {
        return translates.containsKey(main) ? translates.get(main).getTranslation(lang, id, target, replace) : "";
    }

}

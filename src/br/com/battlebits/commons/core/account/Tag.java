package br.com.battlebits.commons.core.account;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;
import net.md_5.bungee.api.ChatColor;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

public enum Tag {

    DONO("§4§l§%owner%§§4", Group.DONO, false), //
    ESTRELA("§1§l§%star%§§1", Group.DONO, false), //
    ADMIN("§c§l§%admin%§§c", Group.ADMIN, false), //
    MANAGER("§c§l§%manager%§§c", Group.MANAGER, false), //
    MODPLUS("§5§l§%modplus%§§5", Group.MODPLUS, false), //
    MOD("§5§l§%mod%§§5", Group.MOD, false), //
    TRIAL("§5§l§%trial%§§5", Group.TRIAL, false), //
    HELPER("§9§l§%helper%§§9", Group.HELPER, false), //
    STAFF("§e§l§%staff%§§e", Group.STAFF, false), //
    BUILDER("§e§l§%builder%§§e", Group.BUILDER, true), //
    DEV("§3§l§%developer%§§3", Group.DEV, true), //
    YOUTUBERPLUS("§3§l§%youtuberplus%§§3", Group.YOUTUBERPLUS, true), //
    YOUTUBER("§b§l§%youtuber%§§b", Group.YOUTUBER, true), //
    ULTIMATE("§d§lULTIMATE§d", Group.ULTIMATE, false), //
    PREMIUM("§6§lPREMIUM§6", Group.PREMIUM, false), //
    LIGHT("§a§lLIGHT§a", Group.LIGHT, false), //
    TORNEIO("§1§l§%tournament%§§1", Group.ADMIN, false), //
    WINNER("§2§l§%winner%§§2", Group.ADMIN, false), //
    NORMAL("§7", Group.NORMAL, false);

    private String prefix;
    private Group groupToUse;
    private boolean isExclusive;

    Tag(String prefix, Group toUse, boolean exclusive) {
        this.prefix = prefix;
        this.groupToUse = toUse;
        this.isExclusive = exclusive;
    }

    public String getPrefix() {
        return prefix;
    }

    public Group getGroupToUse() {
        return groupToUse;
    }

    public boolean isExclusive() {
        return isExclusive;
    }

    public String getPrefix(Language language) {
        String prefix = this.prefix;
        Matcher matcher = T.getPattern().matcher(prefix);
        while (matcher.find()) {
            String replace = matcher.group(), id = matcher.group(2).toLowerCase();
            prefix = prefix.replace(replace, BattlebitsAPI.getTranslate().getTranslation(language, id).toUpperCase());
        }
        return prefix;
    }

    private static final Map<String, Tag> TAG_MAP;

    static {
        Map<String, Tag> map = new ConcurrentHashMap<>();
        for (Tag tag : Tag.values()) {
            map.put(tag.name().toLowerCase(), tag);
            for (Language lang : Language.values()) {
                String prefix = ChatColor.stripColor(tag.getPrefix(lang));
                map.put(prefix.toLowerCase(), tag);
            }
        }
        TAG_MAP = Collections.unmodifiableMap(map);
    }

    /**
     *
     * @param name String
     * @return Tag
     */
    public static Tag getByName(String name) {
        Objects.requireNonNull(name, "Parameter 'name' is null.");
        return TAG_MAP.get(name.toLowerCase());
    }

}
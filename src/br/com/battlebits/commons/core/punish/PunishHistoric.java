package br.com.battlebits.commons.core.punish;

import java.util.ArrayList;
import java.util.List;

public class PunishHistoric {
	private List<Ban> banHistory;
	private List<Mute> muteHistory;
	private List<Kick> antihackKickHistory;

	public PunishHistoric() {
		banHistory = new ArrayList<>();
		muteHistory = new ArrayList<>();
		antihackKickHistory = new ArrayList<>();
	}

	public Ban getActualBan() {
		for (Ban ban : banHistory) {
			if (ban.isUnbanned())
				continue;
			if (ban.hasExpired())
				continue;
			return ban;
		}
		return null;
	}

	public Mute getActualMute() {
		for (Mute mute : muteHistory) {
			if (mute.isUnmuted())
				continue;
			if (mute.hasExpired())
				continue;
			return mute;
		}
		return null;
	}

	public boolean shouldAutoban() {
		int count = 0;
		for (Kick kick : antihackKickHistory) {
			if ((System.currentTimeMillis() - kick.getKickTime()) > 2 * 60 * 60 * 1000)
				continue;
			++count;
		}
		return count >= 2;
	}


	public List<Ban> getBanHistory() {
		return banHistory;
	}

	public List<Mute> getMuteHistory() {
		return muteHistory;
	}

	public List<Kick> getAntihackKickHistory() {
		if(antihackKickHistory == null)
			antihackKickHistory = new ArrayList<>();
		return antihackKickHistory;
	}
}

package br.com.battlebits.commons.core.data;

import java.util.Map;
import java.util.UUID;

import com.google.gson.JsonObject;
import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.party.Party;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class DataParty extends Data {

    public static <T> T getRedisParty(UUID owner, Class<T> clazz) {
        String key = "party:" + owner.toString();
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            if (jedis.exists(key)) {
                if (jedis.ttl(key) >= 0)
                    jedis.persist(key);
                Map<String, String> map = jedis.hgetAll(key);
                if (map != null && !map.isEmpty()) {
                    return mapToObject(map, clazz);
                }
            }
        }
        return null;
    }

    public static void loadParty(Party party) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            Pipeline pipeline = jedis.pipelined();
            JsonObject publish = new JsonObject();
            publish.add("value", jsonTree(party));
            publish.addProperty("action", "load");
            publish.addProperty("source", BattlebitsAPI.getServerId());
            pipeline.publish("party-action", publish.toString());
            jedis.sync();
        }
    }

    public static void unloadParty(Party party) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            Pipeline pipeline = jedis.pipelined();
            JsonObject publish = new JsonObject();
            publish.addProperty("action", "unload");
            publish.addProperty("owner", party.getOwner().toString());
            publish.addProperty("source", BattlebitsAPI.getServerId());
            pipeline.publish("party-action", publish.toString());
            jedis.sync();
        }
    }

    public static void saveRedisParty(Party party) {
        String key = "party:" + party.getOwner().toString();
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            Pipeline pipeline = jedis.pipelined();
            pipeline.hmset(key, objectToMap(party));
            jedis.sync();
        }
    }

    public static void saveRedisPartyField(Party party, String field) {
        JsonObject jsonTree = jsonTree(party);
        if (jsonTree.has(field)) {
            try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
                Pipeline pipeline = jedis.pipelined();
                JsonObject publish = new JsonObject();
                publish.addProperty("owner", party.getOwner().toString());
                publish.addProperty("source", BattlebitsAPI.getServerId());
                publish.addProperty("field", field);
                publish.add("value", jsonTree.get(field));
                pipeline.publish("party-field", publish.toString());
                pipeline.sync();
            }
        }
    }

    public static void expire(Party party) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            BattlebitsAPI.debug("REDIS > EXPIRE 300");
            jedis.expire(party.getID(), 300);
        }
    }

    public static void disbandParty(Party party) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            BattlebitsAPI.debug("REDIS > DELETE");
            Pipeline pipe = jedis.pipelined();
            pipe.del(party.getID());
            jedis.sync();
        }
    }
}

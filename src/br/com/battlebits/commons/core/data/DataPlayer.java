package br.com.battlebits.commons.core.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import br.com.battlebits.commons.util.GeoIpUtils.IpInfo;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.account.BukkitPlayer;
import br.com.battlebits.commons.core.account.BattlePlayer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class DataPlayer extends Data {

    public static BattlePlayer getPlayer(UUID uuid) {
        BattlePlayer player = BattlebitsAPI.getAccountCommon().getBattlePlayer(uuid);
        if (player == null) {
            player = getRedisPlayer(uuid);
            if (player == null)
                player = getMongoPlayer(uuid);
        }
        return player;
    }

    public static BattlePlayer getMongoPlayer(UUID uuid) {
        MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
        if (found == null) {
            return null;
        }
        return BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), BattlePlayer.class);
    }

    public static BattlePlayer createIfNotExistMongo(UUID uuid, String name, String address, IpInfo ipinfo) {
        try {
            MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
            MongoCollection<Document> collection = database.getCollection("account");

            Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
            BattlePlayer player;
            if (found == null) {
                player = new BattlePlayer(name, uuid, address, ipinfo);
                found = Document.parse(BattlebitsAPI.getGson().toJson(player));
                collection.insertOne(found);
                BattlebitsAPI.debug("MONGO > INSERTED");
            } else {
                player = BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), BattlePlayer.class);
                BattlebitsAPI.debug("MONGO > LOADED");
            }
            return player;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static BattlePlayer getRedisPlayer(UUID uuid) {
        BattlePlayer player;
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            if (!jedis.exists("account:" + uuid.toString()))
                return null;
            Map<String, String> fields = jedis.hgetAll("account:" + uuid.toString());
            if (fields == null || fields.isEmpty() || fields.size() < 40)
                return null;
            player = mapToObject(fields, BattlePlayer.class);
        }
        return player;
    }

    public static boolean checkChallenger(UUID uuid) {
        MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        MongoCursor<Document> iterator = collection.find(Filters.eq("league", "CHALLENGER")).sort(new Document("xp", -1)).limit(100).iterator();
        while(iterator.hasNext()) {
            if(iterator.next().getString("uniqueId").equals(uuid.toString()))
                return true;
        }
        return false;
    }

    public static List<UUID> getChallengerList() {
        MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        MongoCursor<Document> iterator = collection.find(Filters.eq("league", "CHALLENGER")).sort(new Document("xp", -1)).limit(100).iterator();
        List<UUID> uuids = new ArrayList<>();
        while(iterator.hasNext()) {
            uuids.add(UUID.fromString(iterator.next().getString("uniqueId")));
        }
        return uuids;
    }

    public static BukkitPlayer getMongoBukkitPlayer(UUID uuid) {
        MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
        if (found == null) {
            return null;
        }
        return BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), BukkitPlayer.class);
    }

    public static void cacheRedisPlayer(UUID uuid) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            BattlebitsAPI.debug("REDIS > EXPIRE 300");
            jedis.expire("account:" + uuid.toString(), 300);
        }
    }

    public static boolean checkCache(UUID uuid) {
        boolean bool = false;
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            String key = "account:" + uuid.toString();
            if (jedis.ttl(key) >= 0) {
                bool = jedis.persist(key) == 1;
            }
        }
        if (bool)
            BattlebitsAPI.debug("REDIS > SHOULD REMOVE");
        else
            BattlebitsAPI.debug("REDIS > SUB-SERVER");
        return bool;
    }

    public static BukkitPlayer createIfNotExistMongoBukkit(UUID uuid, String name, String address, IpInfo ipinfo) {
        MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
        BukkitPlayer player;
        if (found == null) {
            player = new BukkitPlayer(name, uuid, address, ipinfo);
            found = Document.parse(BattlebitsAPI.getGson().toJson(player));
            collection.insertOne(found);
            BattlebitsAPI.debug("MONGO > INSERTED");
        } else {
            player = BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), BukkitPlayer.class);
            BattlebitsAPI.debug("MONGO > LOADED");
        }
        return player;
    }

    public static BukkitPlayer getRedisBukkitPlayer(UUID uuid) {
        BukkitPlayer player;
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            Map<String, String> fields = jedis.hgetAll("account:" + uuid.toString());
            if (fields == null || fields.isEmpty() || fields.size() < 40)
                return null;
            /*/JsonObject obj = new JsonObject();
            for (Entry<String, String> entry : fields.entrySet()) {
                try {
                    obj.add(entry.getKey(), BattlebitsAPI.getParser().parse(entry.getValue()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            player = BattlebitsAPI.getGson().fromJson(obj.toString(), BukkitPlayer.class);*/
            player = mapToObject(fields, BukkitPlayer.class);
        }
        return player;
    }

    public static void saveRedisPlayer(BattlePlayer player) {
        /*/JsonObject jsonObject = BattlebitsAPI.getParser().parse(gson.toJson(player)).getAsJsonObject();
        Map<String, String> playerElements = new HashMap<>();
        for (Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            playerElements.put(entry.getKey(), gson.toJson(entry.getValue()));
        }*/
        BattlebitsAPI.getPlatform().runAsync(() -> {
            try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
                jedis.hmset("account:" + player.getUniqueId().toString(), objectToMap(player));
            }
            BattlebitsAPI.debug("REDIS > SAVE SUCCESS");
        });
    }

    public static void saveBattlePlayer(BattlePlayer player, String fieldName) {
        BattlebitsAPI.getPlatform().runAsync(() -> {
            saveBattleFieldRedis(player, fieldName);
            saveBattleFieldMongo(player, fieldName);
        });
    }

    private static void saveBattleFieldRedis(BattlePlayer player, String fieldName) {
        /*JsonObject jsonObject = BattlebitsAPI.getParser().parse(gson.toJson(player)).getAsJsonObject();
        if (!jsonObject.has(fieldName))
            return;
        JsonElement element = jsonObject.get(fieldName);*/
        JsonObject tree = BattlebitsAPI.getGson().toJsonTree(player).getAsJsonObject();
        if (tree.has(fieldName)) {
            JsonElement element = tree.get(fieldName);
            try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
                Pipeline pipe = jedis.pipelined();
                jedis.hset("account:" + player.getUniqueId().toString(), fieldName, elementToString(element));

                JsonObject json = new JsonObject();
                json.add("uniqueId", new JsonPrimitive(player.getUniqueId().toString()));
                json.add("source", new JsonPrimitive(BattlebitsAPI.getServerId()));
                json.add("field", new JsonPrimitive(fieldName));
                json.add("value", element);
                pipe.publish("account-field", json.toString());

                pipe.sync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void saveBattleFieldMongo(BattlePlayer player, String fieldName) {
        try {
            JsonObject object = jsonTree(player);
            if (object.has(fieldName)) {
                Object value = elementToBson(object.get(fieldName));
                MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
                MongoCollection<Document> collection = database.getCollection("account");
                collection.updateOne(Filters.eq("uniqueId", player.getUniqueId().toString()),
                        new Document("$set", new Document(fieldName, value)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveConfigField(BattlePlayer player, String fieldName) {
        BattlebitsAPI.getPlatform().runAsync(() -> {
            saveBattleFieldRedis(player, "configuration");
            saveConfigFieldMongo(player, fieldName);
        });
    }

    private static void saveConfigFieldMongo(BattlePlayer player, String fieldName) {
        try {
            JsonObject object = jsonTree(player.getConfiguration());
            if (object.has(fieldName)) {
                Object value = elementToBson(object.get(fieldName));
                MongoDatabase database = BattlebitsAPI.getCommonsMongo().getClient().getDatabase("commons");
                MongoCollection<Document> collection = database.getCollection("account");
                collection.updateOne(Filters.eq("uniqueId", player.getUniqueId().toString()),
                        new Document("$set", new Document("configuration." + fieldName, value)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveCompleteBattlePlayer(BattlePlayer player) {
        // TODO
    }
}

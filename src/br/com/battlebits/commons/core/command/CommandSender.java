package br.com.battlebits.commons.core.command;

import java.util.UUID;

import net.md_5.bungee.api.chat.BaseComponent;

public interface CommandSender {

    UUID getUniqueId();

    void sendMessage(String str);

    void sendMessage(BaseComponent str);

    void sendMessage(BaseComponent[] fromLegacyText);
}

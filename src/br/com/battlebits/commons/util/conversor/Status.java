package br.com.battlebits.commons.util.conversor;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by gustavo on 11/06/17.
 */
@Getter
public class Status {
    @Setter
    private UUID uniqueId;
    private int kills = 0;
    private int deaths = 0;
    private int killstreak = 0;
    private Set<String> kits = new HashSet<>();
    private boolean scoreboardEnabled = true;
    private boolean canResetKD = false;


}

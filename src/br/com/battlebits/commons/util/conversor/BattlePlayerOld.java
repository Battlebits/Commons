package br.com.battlebits.commons.util.conversor;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.account.AccountConfiguration;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.account.RankType;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.util.timezone.TimeZone;
import br.com.battlebits.commons.util.timezone.TimeZoneConversor;
import lombok.Getter;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

 public class BattlePlayerOld {

 // INFORMAOES DA CONTA
 private String userName;
 private UUID uuid;
 private String fakeName;

 // DADOS DA CONTA
 private int doubleXpMultiplier = 0;

 private long lastActivatedMultiplier = Long.MIN_VALUE;
 private long lastVIPMultiplierReceived = Long.MIN_VALUE;
  @Getter
  private LeagueOld liga = LeagueOld.UNRANKED;

 private String lastIpAddress;

 // PLAYING
 private long onlineTime = 0;
 @Getter
 private long joinTime;
 private long lastLoggedIn;
 private long firstTimePlaying;


 // GRUPOS
 private Map<RankType, Long> ranks = new HashMap<>();



 // CONFIGURACOES
 private AccountConfiguration configuration = new AccountConfiguration();

 private GameStatus gameStatus = new GameStatus();

 public BattlePlayerOld() {

 }

 public BattlePlayerOld(String userName, UUID uuid, InetSocketAddress ipAddress, String countryCode, String timeZoneCode) {
 this.userName = userName;
 this.uuid = uuid;
 this.fakeName = userName;

 this.lastIpAddress = ipAddress.getHostString();

 this.lastLoggedIn = TimeZoneConversor.getCurrentMillsTimeIn(TimeZone.GMT0);
 this.firstTimePlaying = TimeZoneConversor.getCurrentMillsTimeIn(TimeZone.GMT0);
 }

 public String getUserName() {
 return userName;
 }

 public UUID getUuid() {
 return uuid;
 }

 public String getFakeName() {
 return fakeName;
 }

 public int getDoubleXpMultiplier() {
 return doubleXpMultiplier;
 }

 public long getOnlineTime() {
 return (System.currentTimeMillis() - joinTime) + onlineTime;
 }

 public long getLastLoggedIn() {
 return lastLoggedIn;
 }

 public long getFirstTimePlaying() {
 return firstTimePlaying;
 }

 public String getLastIpAddress() {
 return lastIpAddress;
 }

 public AccountConfiguration getConfiguration() {
 return configuration;
 }




 public void setDoubleXpMultiplier(int doubleXpMultiplier) {
 this.doubleXpMultiplier = doubleXpMultiplier;
 }

 public Map<RankType, Long> getRanks() {
 return ranks;
 }

 public GameStatus getGameStatus() {
 if (gameStatus == null)
 gameStatus = new GameStatus();
 return gameStatus;
 }



 public long getLastActivatedMultiplier() {
 return lastActivatedMultiplier;
 }



 public void setFakeName(String fakeName) {
 this.fakeName = fakeName;
 }


 public void activateDoubleXp() {
 removeDoubleXpMultiplier(1);
 lastActivatedMultiplier = System.currentTimeMillis() + BattlebitsAPI.MULTIPLIER_DURATION;
 }

 public void addDoubleXpMultiplier(int i) {
 doubleXpMultiplier += i;
 }

 public void removeDoubleXpMultiplier(int i) {
 doubleXpMultiplier -= i;
 if (doubleXpMultiplier < 0)
 doubleXpMultiplier = 0;
 }


 public boolean isDoubleXPActivated() {
 return System.currentTimeMillis() < lastActivatedMultiplier;
 }


 public void updateRanks(Map<RankType, Long> ranks) {
 this.ranks = ranks;
 }

 public void setConfiguration(AccountConfiguration config) {
 this.configuration = config;
 }

 public void setGameStatus(GameStatus status) {
 this.gameStatus = status;
 }

 public void updateGameStatus(GameStatus gameStatus) {
 this.gameStatus = gameStatus;
 }

 public void sendMessage(String translateId) {
 this.sendMessage(null, translateId);
 }

 public void sendMessage(String tagId, String translateId) {
 this.sendMessage(tagId, translateId, null);
 }

 public void sendMessage(String translateId, Map<String, String> replaces) {
 this.sendMessage(null, translateId, replaces);
 }

 public void sendMessage(String tagId, String translateId, Map<String, String> replaces) {

 }


 public static Language getLanguage(UUID uuid) {
 return getPlayer(uuid).getLanguage();
 }

 public static BattlePlayer getPlayer(UUID uuid) {
 return BattlebitsAPI.getAccountCommon().getBattlePlayer(uuid);
 }

 }


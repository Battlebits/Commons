package br.com.battlebits.commons.util.conversor;

import br.com.battlebits.commons.BattlebitsAPI;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gustavo on 11/06/17.
 */
public class GameStatus {

    private Map<String, String> minigameStatus; // Minigame Name, Status JSON

    public GameStatus() {
        minigameStatus = new HashMap<>();
    }

    public Map<String, String> getMinigameStatus() {
        return minigameStatus;
    }

    public void setMinigameStatus(Map<String, String> status) {
        this.minigameStatus = status;
    }

    public <T> T getMinigame(GameType type, Class<?> cls) {
        if (!minigameStatus.containsKey(type.getServerId()))
            return null;
        @SuppressWarnings("unchecked")
        T game = (T) BattlebitsAPI.getGson().fromJson(minigameStatus.get(type.getServerId()).replace("\\", "\""), cls);
        return game;
    }

    public void updateMinigame(GameType key, Object mini) {
        updateMinigame(key.getServerId(), BattlebitsAPI.getGson().toJson(mini));
    }

    public void updateMinigame(String key, String mini) {
        minigameStatus.put(key, mini);
    }


}

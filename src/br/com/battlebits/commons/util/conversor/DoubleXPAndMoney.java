package br.com.battlebits.commons.util.conversor;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.backend.mongodb.MongoBackend;
import br.com.battlebits.commons.core.backend.sql.MySQLBackend;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by gustavo on 16/06/17.
 */
public class DoubleXPAndMoney {

    public static void convert() throws Exception {
        HashMap<UUID, BattlePlayerOld> accounts = new HashMap<>();
        HashMap<UUID, BattlePlayer> newAcconts = new HashMap<>();
        MongoBackend mongo = new MongoBackend("localhost", "admin", "gustavo", "6DqblYTJY9r0wsjo", 27017);
        MySQLBackend mysql = new MySQLBackend("localhost", "ycommon", "ycommon", "qhu6fvCu5X8ZH7TY", 3306);
        mysql.startConnection();
        mongo.startConnection();
        PreparedStatement stmt = mysql.prepareStatment("SELECT * FROM `account`");
        ResultSet result = stmt.executeQuery();
        while(result.next()) {
            UUID uuid = null;
            try {
                uuid = getUUIDFromString(result.getString("uuid"));
                BattlePlayerOld player = BattlebitsAPI.getGson().fromJson(result.getString("json"), BattlePlayerOld.class);
                accounts.put(uuid, player);
            } catch (Exception e) {
                System.out.println("Erro com conta " + uuid);
            }
        }
        result.close();
        stmt.close();

        for(BattlePlayerOld old : accounts.values()) {
            if(old.getUuid() == null || old == null)
                continue;
            if(old.getUserName().isEmpty())
                continue;
            if(old.getLiga() == null) {
                continue;
            }
            BattlePlayer player = new BattlePlayer(old);
            if(!player.getName().isEmpty())
                newAcconts.put(old.getUuid(), player);
        }

        MongoDatabase database = mongo.getClient().getDatabase("commons");
        MongoCollection<Document> collection = database.getCollection("account");

        for(BattlePlayer newAccount : newAcconts.values()) {
            collection.updateOne(Filters.eq("uniqueId", newAccount.getUniqueId().toString()), new Document("$inc", new Document("doubleXpMultiplier", newAccount.getDoubleXpMultiplier()).append("money", newAccount.getMoney())), new UpdateOptions().upsert(false));
        }

        mongo.closeConnection();
        mysql.closeConnection();
    }

    public static UUID getUUIDFromString(String id) {
        return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" + id.substring(20, 32));
    }


}
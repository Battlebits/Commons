package br.com.battlebits.commons.util.string;

/**
 * Arquivo criado em 05/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class StringTimeUtils {

    public static String format(int time) {
        if (time >= 3600) {
            int hours = (time / 3600), minutes = (time % 3600) / 60, seconds = (time % 3600) % 60;
            return (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        } else {
            int minutes = (time / 60), seconds = (time % 60);
            return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        }
    }

}

package br.com.battlebits.commons.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.data.Data;
import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

public class GeoIpUtils {

    private static final Map<String, IpInfo> CACHE = new HashMap<>();

    public static IpInfo getIpInfo(String ip) throws IOException {
        if (CACHE.containsKey(ip))
            return CACHE.get(ip);

        IpInfo redis = getIpInfoRedis(ip);
        if (redis != null)
            return CACHE.computeIfAbsent(ip, v -> redis);

        URLConnection con = new URL("http://ip-api.com/json/" + ip).openConnection();
        IpInfo ipInfo = BattlebitsAPI.getGson().fromJson(new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8")), IpInfo.class);
        if (!"success".equals(ipInfo.getStatus()))
            throw new IOException();

        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            Pipeline pipeline = jedis.pipelined();
            pipeline.hmset("ip:" + ip, Data.objectToMap(ipInfo));
            jedis.sync();
        }

        return CACHE.computeIfAbsent(ip, v -> ipInfo);
    }

    private static IpInfo getIpInfoRedis(String ip) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            String key = "ip:" + ip;
            if (jedis.exists(key)) {
                if (jedis.ttl(key) >= 0)
                    jedis.persist(key);
                Map<String, String> data = jedis.hgetAll(key);
                if (data != null && !data.isEmpty()) {
                    return Data.mapToObject(data, IpInfo.class);
                }
            }
        }

        return null;
    }

    public static void invalidate(String ip) {
        try (Jedis jedis = BattlebitsAPI.getCommonsRedis().getPool().getResource()) {
            jedis.expire("ip:" + ip, 300);
        }
    }

    @Getter
    public static class IpInfo {
        private String as;
        private String city;
        private String country;
        private String countryCode;
        private String isp;
        private String lat;
        private String lon;
        private String org;
        private String query;
        private String region;
        private String regionName;
        private String status;
        private String timezone;
        private String zip;
    }
}

package br.com.battlebits.commons.bukkit.redis;

import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.JsonObject;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.bukkit.account.BukkitPlayer;
import br.com.battlebits.commons.bukkit.event.account.PlayerUpdateFieldEvent;
import br.com.battlebits.commons.bukkit.event.account.PlayerUpdatedFieldEvent;
import br.com.battlebits.commons.bukkit.event.redis.RedisPubSubMessageEvent;
import br.com.battlebits.commons.bukkit.party.BukkitParty;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.clan.Clan;
import br.com.battlebits.commons.core.party.Party;
import redis.clients.jedis.JedisPubSub;

public class BukkitPubSubHandler extends JedisPubSub {

    @Override
    public void onMessage(String channel, String message) {
        Bukkit.getPluginManager().callEvent(new RedisPubSubMessageEvent(channel, message));
        if (message.startsWith("{") && message.endsWith("}")) {
            JsonObject obj = (JsonObject) BattlebitsAPI.getParser().parse(message);
            if (obj.has("source") && !obj.get("source").getAsString().equals(BattlebitsAPI.getServerId())) {
                switch (channel) {
                    case "account-field": {
                        if (!obj.has("uniqueId")) break;
                        if (!obj.has("field")) break;
                        if (!obj.has("value")) break;
                        UUID uuid = UUID.fromString(obj.get("uniqueId").getAsString());
                        Player p = BukkitMain.getInstance().getServer().getPlayer(uuid);
                        BukkitPlayer player = (BukkitPlayer) BattlePlayer.getPlayer(uuid);
                        if (p != null && player != null) {
                            try {
                                Field field = getField(BattlePlayer.class, obj.get("field").getAsString());
                                Object object = BattlebitsAPI.getGson().fromJson(obj.get("value"), field.getGenericType());
                                PlayerUpdateFieldEvent event = new PlayerUpdateFieldEvent(p, player, field.getName(), object);
                                Bukkit.getPluginManager().callEvent(event);
                                if (!event.isCancelled()) {
                                    field.set(player, event.getObject());
                                    PlayerUpdatedFieldEvent event2 = new PlayerUpdatedFieldEvent(p, player, field.getName(), object);
                                    Bukkit.getPluginManager().callEvent(event2);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                    case "clan-field": {
                        if (!obj.has("uniqueId")) break;
                        if (!obj.has("field")) break;
                        if (!obj.has("value")) break;
                        UUID uuid = UUID.fromString(obj.get("uniqueId").getAsString());
                        Clan clan = BattlebitsAPI.getClanCommon().getClan(uuid);
                        if (clan != null) {
                            try {
                                Field field = getField(Clan.class, obj.get("field").getAsString());
                                Object object = BattlebitsAPI.getGson().fromJson(obj.get("value"), field.getGenericType());
                                field.set(clan, object);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                    case "party-field": {
                        if (!obj.has("owner")) break;
                        if (!obj.has("field")) break;
                        if (!obj.has("value")) break;
                        UUID owner = UUID.fromString(obj.get("owner").getAsString());
                        Party party = BattlebitsAPI.getPartyCommon().getByOwner(owner);
                        if (party != null) {
                            try {
                                Field field = getField(BukkitParty.class, obj.get("field").getAsString());
                                Object object = BattlebitsAPI.getGson().fromJson(obj.get("value"), field.getGenericType());
                                field.set(party, object);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    }
                    case "party": {
                        if (!obj.has("owner")) break;
                        if (!obj.has("action")) break;
                        UUID owner = UUID.fromString(obj.get("owner").getAsString());
                        String action = obj.get("action").getAsString();
                        switch (action) {
                            case "load":
                                if (!obj.has("object")) break;
                                Party party = BattlebitsAPI.getGson().fromJson(obj.get("object"), BukkitParty.class);
                                if (BattlebitsAPI.getPartyCommon().getByOwner(party.getOwner()) == null && party.getOnlineCount() > 0)
                                    BattlebitsAPI.getPartyCommon().loadParty(party);
                                break;
                            case "unload":
                                BattlebitsAPI.getPartyCommon().removeParty(owner);
                                break;
                        }
                        break;
                    }
                }
            }
        }
    }

    private Field getField(Class<?> clazz, String fieldName) {
        while ((clazz != null) && (clazz != Object.class)) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                return field;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }
        return null;
    }
}

package br.com.battlebits.commons.bukkit.injector;

import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe criada em 12/06/17.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class PacketLimiterInjector implements Injector {

    private final Map<Player, PPS> ppsMap = new HashMap<>();

    @Override
    public void inject(BukkitMain main) {
        ProtocolLibrary.getProtocolManager().getAsynchronousManager().registerAsyncHandler(new PacketAdapter(main, ListenerPriority.NORMAL, PacketType.Play.Client.FLYING, PacketType.Play.Client.POSITION) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                Player player = event.getPlayer();
                if (player == null)
                    return;
                PPS pps = ppsMap.computeIfAbsent(player, v -> new PPS());
                if (pps.last > System.currentTimeMillis()) {
                    if (++pps.count > 500) {
                        event.setCancelled(true);
                        if (!pps.kicked) {
                            try {
                                pps.kicked = true;
                                Language language = BattlePlayer.getLanguage(player.getUniqueId());
                                PacketContainer packet = new PacketContainer(PacketType.Play.Server.KICK_DISCONNECT);
                                packet.getChatComponents().write(0, WrappedChatComponent.fromText(T.t(main, language, "max-pps-kick-message")));
                                main.getProcotolManager().sendServerPacket(player, packet);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    pps.last = System.currentTimeMillis() + 1000L;
                    pps.count = 0;
                }
            }
        }).start();

        main.getServer().getPluginManager().registerEvents(new Listener() {

            @EventHandler(priority = EventPriority.MONITOR)
            public void onPlayerQuit(PlayerQuitEvent event) {
                ppsMap.remove(event.getPlayer());
            }

        }, main);
    }

    protected class PPS {

        private boolean kicked;
        private long last;
        private int count;

    }

}

package br.com.battlebits.commons.bukkit.injector;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.util.string.StringLoreUtils;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import com.google.common.base.Splitter;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

public class TranslationInjector implements Injector {

    @Override
    public void inject(BukkitMain plugin) {
        plugin.getProcotolManager()
                .addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, //
                        PacketType.Play.Server.CHAT, //
                        PacketType.Play.Server.WINDOW_ITEMS, //
                        PacketType.Play.Server.SET_SLOT, //
                        PacketType.Play.Server.OPEN_WINDOW, //
                        PacketType.Play.Server.UPDATE_SIGN, //
                        PacketType.Play.Server.SCOREBOARD_OBJECTIVE, //
                        PacketType.Play.Server.SCOREBOARD_TEAM, //
                        PacketType.Play.Server.SCOREBOARD_SCORE, //
                        PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER, //
                        PacketType.Play.Server.SPAWN_ENTITY_LIVING, //
                        PacketType.Play.Server.ENTITY_METADATA, //
                        PacketType.Play.Server.TITLE) {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if (event.getPlayer() == null)
                            return;
                        if (event.getPlayer().getUniqueId() == null)
                            return;
                        if (event.getPacket() == null)
                            return;
                        if (event.isReadOnly())
                            return;
                        Language lang = BattlePlayer.getLanguage(event.getPlayer().getUniqueId());
                        if (event.getPacketType() == PacketType.Play.Server.CHAT) {
                            PacketContainer packet = event.getPacket().deepClone();
                            for (int i = 0; i < packet.getChatComponents().size(); i++) {
                                WrappedChatComponent chatComponent = packet.getChatComponents().read(i);
                                if (chatComponent != null) {
                                    packet.getChatComponents().write(i, WrappedChatComponent.fromJson(translate(lang, chatComponent.getJson())));
                                }
                            }
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.WINDOW_ITEMS) {
                            PacketContainer packet = event.getPacket().deepClone();
                            for (ItemStack item : packet.getItemArrayModifier().read(0)) {
                                if (item == null) {
                                    continue;
                                }
                                translate(lang, item);
                            }
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.SET_SLOT) {
                            PacketContainer packet = event.getPacket().deepClone();
                            ItemStack item = packet.getItemModifier().read(0);
                            packet.getItemModifier().write(0, translate(lang, item));
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.TITLE) {
                            PacketContainer packet = event.getPacket().deepClone();
                            WrappedChatComponent component = event.getPacket().getChatComponents().read(0);
                            if (component == null)
                                return;
                            packet.getChatComponents().write(0, WrappedChatComponent.fromJson(translate(lang, component.getJson())));
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.SCOREBOARD_SCORE) {
                            PacketContainer packet = event.getPacket().deepClone();
                            String message = event.getPacket().getStrings().read(0);
                            packet.getStrings().write(0, translate(lang, message));
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.OPEN_WINDOW) {
                            PacketContainer packet = event.getPacket().deepClone();
                            WrappedChatComponent component = event.getPacket().getChatComponents().read(0);
                            JsonElement element = BattlebitsAPI.getParser().parse(component.getJson());
                            if (!(element instanceof JsonObject && ((JsonObject) element).has("translate"))) {
                                String message = translate(lang, element.getAsString());
                                message = message.substring(0, message.length() > 32 ? 32 : message.length());
                                packet.getChatComponents().write(0, WrappedChatComponent.fromText(message));
                                event.setPacket(packet);
                            }
                        } else if (event.getPacketType() == PacketType.Play.Server.SCOREBOARD_OBJECTIVE) {
                            PacketContainer packet = event.getPacket().deepClone();
                            String message = event.getPacket().getStrings().read(1);
                            packet.getStrings().write(1, translate(lang, message));
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.SCOREBOARD_TEAM) {
                            PacketContainer packet = event.getPacket().deepClone();
                            String pre = packet.getStrings().read(2);
                            String su = packet.getStrings().read(3);
                            boolean matched = false;
                            Matcher matcher = T.getPattern().matcher(pre);
                            while (matcher.find()) {
                                pre = pre.replace(matcher.group(), T.getTranslation(lang, matcher.group(2)));
                                matched = true;
                            }
                            matcher = T.getPattern().matcher(su);
                            while (matcher.find()) {
                                su = su.replace(matcher.group(), T.getTranslation(lang, matcher.group(2)));
                                matched = true;
                            }
                            if (matched) {
                                if (pre.length() <= 16 && su.length() <= 16) {
                                    packet.getStrings().write(2, pre);
                                    packet.getStrings().write(3, su);
                                    event.setPacket(packet);
                                    return;
                                }
                            }
                            String text = packet.getStrings().read(2) + packet.getStrings().read(3);
                            matcher = T.getPattern().matcher(text);
                            matched = false;
                            while (matcher.find()) {
                                text = text.replace(matcher.group(), T.getTranslation(lang, matcher.group(2)));
                                matched = true;
                            }
                            if (!matched)
                                return;
                            String prefix, suffix;
                            Iterator<String> iterator = Splitter.fixedLength(16).split(text).iterator();
                            String str = iterator.next();
                            if (str.endsWith("§")) {
                                str = str.substring(0, str.length() - 1);
                                prefix = str;
                                if (iterator.hasNext()) {
                                    String next = iterator.next();
                                    if (!next.startsWith("§")) {
                                        String str2 = "§" + next;
                                        if (str2.length() > 16)
                                            str2 = str2.substring(0, 16);
                                        suffix = str2;
                                    } else {
                                        suffix = next;
                                    }
                                } else {
                                    suffix = "";
                                }
                            } else if (iterator.hasNext()) {
                                String next = iterator.next();
                                if (!next.startsWith("§")) {
                                    String colors = org.bukkit.ChatColor.getLastColors(str);
                                    String str3 = colors + next;
                                    if (str3.length() > 16)
                                        str3 = str3.substring(0, 16);
                                    prefix = str;
                                    suffix = str3;
                                } else {
                                    prefix = str;
                                    suffix = next;
                                }
                            } else {
                                prefix = str;
                                suffix = "";
                            }
                            packet.getStrings().write(2, prefix);
                            packet.getStrings().write(3, suffix);
                            event.setPacket(packet);

                        } else if (event.getPacketType() == PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER) {
                            PacketContainer packet = event.getPacket().deepClone();
                            WrappedChatComponent header = packet.getChatComponents().read(0);
                            WrappedChatComponent footer = packet.getChatComponents().read(1);
                            if (header != null)
                                packet.getChatComponents().write(0, WrappedChatComponent.fromJson(translate(lang, header.getJson())));
                            if (footer != null)
                                packet.getChatComponents().write(1, WrappedChatComponent.fromJson(translate(lang, footer.getJson())));
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.UPDATE_SIGN) {
                            PacketContainer packet = event.getPacket().deepClone();
                            for (int i = 0; i < packet.getChatComponents().size(); i++) {
                                WrappedChatComponent component = packet.getChatComponents().read(i);
                                packet.getChatComponents().write(i, WrappedChatComponent.fromJson(translate(lang, component.getJson())));
                            }
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.ENTITY_METADATA) {
                            PacketContainer packet = event.getPacket().deepClone();
                            List<WrappedWatchableObject> objects = packet.getWatchableCollectionModifier().read(0);
                            for (WrappedWatchableObject obj : objects) {
                                if (obj.getIndex() == 2) {
                                    String str = (String) obj.getRawValue();
                                    str = translate(lang, str);
                                    obj.setValue(str);
                                    break;
                                }
                            }
                            event.setPacket(packet);
                        } else if (event.getPacketType() == PacketType.Play.Server.SPAWN_ENTITY_LIVING) {
                            PacketContainer packet = event.getPacket();
                            int type = packet.getIntegers().read(1);

                            if (type != EntityType.ARMOR_STAND.getTypeId()) {
                                return;
                            }
                            PacketContainer packetClone = event.getPacket().deepClone();
                            List<WrappedWatchableObject> objects = packetClone.getDataWatcherModifier().read(0).getWatchableObjects();
                            for (WrappedWatchableObject obj : objects) {
                                if (obj.getIndex() == 2) {
                                    String str = (String) obj.getRawValue();
                                    str = translate(lang, str);
                                    obj.setValue(str);
                                    break;
                                }
                            }
                            event.setPacket(packetClone);
                        }
                    }

                });
    }

    private String translate(Language lang, String string) {
        if (string != null && !string.isEmpty()) {
            Matcher matcher = T.getPattern().matcher(string);
            while (matcher.find()) {
                String replace = matcher.group(), id = matcher.group(2).toLowerCase();
                string = string.replace(replace, T.getTranslation(lang, id));
            }
        }
        return string;
    }

    private ItemStack translate(Language lang, ItemStack item) {
        if (item != null && item.getType() != Material.AIR) {
            if (item.hasItemMeta()) {
                ItemMeta meta = item.getItemMeta();
                if (meta.hasDisplayName()) {
                    String name = meta.getDisplayName();
                    meta.setDisplayName(translate(lang, name));
                }
                if (meta.hasLore()) {
                    List<String> lore = new ArrayList<>();
                    for (String line : meta.getLore()) {
                        line = translate(lang, line);
                        if (line.contains("\n")) {
                            String[] split = line.split("\n");
                            for (int i = 0; i < split.length; i++) {
                                lore.addAll(StringLoreUtils.formatForLore(split[i]));
                            }
                        } else {
                            lore.addAll(StringLoreUtils.formatForLore(line));
                        }
                    }
                    meta.setLore(lore);
                }
                item.setItemMeta(meta);
            }
        }
        return item;
    }

}

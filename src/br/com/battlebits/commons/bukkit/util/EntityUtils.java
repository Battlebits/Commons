package br.com.battlebits.commons.bukkit.util;

import com.comphenix.protocol.utility.MinecraftReflection;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.*;

import java.lang.reflect.Field;

/**
 * Arquivo criado em 04/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class EntityUtils {

    public static synchronized int next() {
        try {
            Class<?> clazz = MinecraftReflection.getEntityClass();
            Field field = clazz.getDeclaredField("entityCount");
            field.setAccessible(true);
            int id = field.getInt(null);
            field.set(null, id + 1);
            return id;
        } catch (Exception e) {
            return -1;
        }
    }

    public static void clearDrops() {
        for (World world : Bukkit.getWorlds()) {
            for (Entity entity : world.getEntities()) {
                if (entity instanceof Item) {
                    entity.remove();
                }
            }
        }
    }

    public static void clearEntities() {
        for (World world : Bukkit.getWorlds()) {
            for (Entity entity : world.getEntities()) {
                if (entity instanceof Item || entity instanceof Animals || entity instanceof Monster || entity instanceof NPC) {
                    entity.remove();
                }
            }
        }
    }

}

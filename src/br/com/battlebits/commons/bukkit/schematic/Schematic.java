package br.com.battlebits.commons.bukkit.schematic;

import br.com.battlebits.commons.bukkit.schematic.jnbt.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Arquivo criado em 04/06/2017.
 * Desenvolvido por:
 *
 * @author Luãn Pereira.
 */
public class Schematic {

    private InputStream stream;
    private List<Block> remove;

    private short[] blocks;
    private byte[] blocksData;

    private short width, length, height;
    private int offsetX, offsetY, offsetZ;

    public Schematic(File file) throws IOException {
        this(new FileInputStream(file));
    }

    public Schematic(InputStream stream) {
        try {
            NBTInputStream nbtStream = new NBTInputStream(new GZIPInputStream(stream));

            // Schematic tag
            NamedTag rootTag = nbtStream.readNamedTag();
            nbtStream.close();
            if (!rootTag.getName().equals("Schematic")) {
                throw new Exception("Tag \"Schematic\" does not exist or is not first");
            }

            CompoundTag schematicTag = (CompoundTag) rootTag.getTag();

            // Check
            Map<String, Tag> schematic = schematicTag.getValue();
            if (!schematic.containsKey("Blocks")) {
                throw new Exception("Schematic file is missing a \"Blocks\" tag");
            }

            // Get information
            this.width = getChildTag(schematic, "Width", ShortTag.class).getValue();
            this.length = getChildTag(schematic, "Length", ShortTag.class).getValue();
            this.height = getChildTag(schematic, "Height", ShortTag.class).getValue();

            this.offsetX = getChildTag(schematic, "WEOffsetX", IntTag.class).getValue();
            this.offsetY = getChildTag(schematic, "WEOffsetY", IntTag.class).getValue();
            this.offsetZ = getChildTag(schematic, "WEOffsetZ", IntTag.class).getValue();

            // Check type of Schematic
            String materials = getChildTag(schematic, "Materials", StringTag.class).getValue();
            if (!materials.equals("Alpha")) {
                throw new Exception("Schematic file is not an Alpha schematic");
            }

            // Get blocks
            byte[] blockId = getChildTag(schematic, "Blocks", ByteArrayTag.class).getValue();
            this.blocksData = getChildTag(schematic, "Data", ByteArrayTag.class).getValue();
            this.blocks = new short[blockId.length]; // Have to later combine IDs

            // We support 4096 block IDs using the same method as vanilla Minecraft, where
            // the highest 4 bits are stored in a separate byte array.
            byte[] addId = new byte[0];
            if (schematic.containsKey("AddBlocks")) {
                addId = getChildTag(schematic, "AddBlocks", ByteArrayTag.class).getValue();
            }

            // Combine the AddBlocks data with the first 8-bit block ID
            for (int index = 0; index < blockId.length; index++) {
                if ((index >> 1) >= addId.length) { // No corresponding AddBlocks index
                    this.blocks[index] = (short) (blockId[index] & 0xFF);
                } else {
                    if ((index & 1) == 0) {
                        this.blocks[index] = (short) (((addId[index >> 1] & 0x0F) << 8) + (blockId[index] & 0xFF));
                    } else {
                        this.blocks[index] = (short) (((addId[index >> 1] & 0xF0) << 4) + (blockId[index] & 0xFF));
                    }
                }
            }

            this.remove = new ArrayList<>();
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public void paste(Location location) {
        this.paste(location, false);
    }

    @SuppressWarnings("deprecation")
    public void paste(Location location, boolean noAir) {
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                for (int z = 0; z < length; ++z) {
                    int index = y * width * length + z * width + x;
                    if (noAir && blocks[index] == 0)
                        continue;
                    Block block = location.clone().add(offsetX, offsetY, offsetZ).add(x, y, z).getBlock();
                    block.setTypeIdAndData(blocks[index], blocksData[index], true);
                    if (block.getType() != Material.AIR)
                        remove.add(block);
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    public boolean undo() {
        if (!remove.isEmpty()) {
            remove.forEach(b -> b.setTypeIdAndData(0, (byte) 0, true));
            remove.clear();
            return true;
        }
        return false;
    }

    private <T extends Tag> T getChildTag(Map<String, Tag> items, String key, Class<T> expected) throws Exception {
        if (!items.containsKey(key)) {
            throw new Exception("Schematic file is missing a \"" + key + "\" tag");
        }
        Tag tag = items.get(key);
        if (!expected.isInstance(tag)) {
            throw new Exception(key + " tag is not of tag type " + expected.getName());
        }
        return expected.cast(tag);
    }

}

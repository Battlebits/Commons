package br.com.battlebits.commons.bukkit.scheduler;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.bossbar.BossBarAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.util.string.AnimatedString;
import org.bukkit.Bukkit;

import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;

public class UpdateScheduler implements Runnable {

    private long currentTick;
    private AnimatedString bossbar;

    public UpdateScheduler() {
        bossbar = new AnimatedString(BattlebitsAPI.SERVER_ADDRESS.toUpperCase(), "§6§l", "§e§l", "§7§l", 0);
    }

    @Override
    public void run() {
        currentTick++;
        Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.TICK, currentTick));

        if (currentTick % 20 == 0) {
            Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.SECOND, currentTick));
        }

        if (currentTick % 1200 == 0) {
            Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.MINUTE, currentTick));
        }

        if (currentTick % 2 == 0) {
            if (BukkitMain.getInstance().isBossbarAddress()) {
                String next = bossbar.next();
                Bukkit.getOnlinePlayers().forEach(p -> BossBarAPI.setBar(p, next, 100F));
            }
        }
    }
}

package br.com.battlebits.commons.bukkit.command.register;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.core.command.CommandSender;
import br.com.battlebits.commons.core.data.DataPlayer;
import org.bukkit.entity.Player;

import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.bukkit.command.BukkitCommandArgs;
import br.com.battlebits.commons.bukkit.menu.account.AccountMenu;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;

import java.util.UUID;

public class AccountCommand implements CommandClass {

	@Command(name = "account", aliases = { "acc", "conta" }, runAsync = true)
	public void account(BukkitCommandArgs cmdArgs) {
		CommandSender sender = cmdArgs.getSender();
		BattlePlayer opener = BattlePlayer.getPlayer(cmdArgs.getPlayer().getUniqueId());
		String[] args = cmdArgs.getArgs();

		UUID uuid = cmdArgs.getPlayer().getUniqueId();

		if(args.length > 0)
			uuid = BattlebitsAPI.getUUIDOf(args[0]);
		Language lang = opener.getLanguage();
		final String accountPrefix = T.t(BukkitMain.getInstance(),lang, "command-account-prefix") + " ";

		if (uuid == null) {
			sender.sendMessage(accountPrefix + T.t(BukkitMain.getInstance(),lang, "player-not-exist"));
			return;
		}
		BattlePlayer player = BattlebitsAPI.getAccountCommon().getBattlePlayer(uuid);
		if (player == null) {
			try {
				player = DataPlayer.getPlayer(uuid);
			} catch (Exception e) {
				e.printStackTrace();
				sender.sendMessage(accountPrefix + T.t(BukkitMain.getInstance(),lang, "cant-request-offline"));
				return;
			}
			if (player == null) {
				sender.sendMessage(accountPrefix + T.t(BukkitMain.getInstance(),lang, "player-never-joined"));
				return;
			}
		}

		AccountMenu.open(cmdArgs.getPlayer(), opener, player);
	}

	@Command(name = "language", aliases = { "lang" })
	public void language(BukkitCommandArgs args) {
		if (!args.isPlayer()) {
			args.getSender().sendMessage("Apenas jogadores");
			return;
		}
		Player p = args.getPlayer();
		if (args.getArgs().length < 1) {
			p.sendMessage("§%command-language-prefix%§ §%command-language-usage%§");
			return;
		}
		Language lang;
		try {
			lang = Language.valueOf(args.getArgs()[0].toUpperCase());
		} catch (Exception e) {
			p.sendMessage("§%command-language-prefix%§ §%command-language-not-language%§");
			return;
		}
		BattlePlayer player = BattlePlayer.getPlayer(p.getUniqueId());
		if (player.getLanguage() == lang) {
			p.sendMessage("§%command-language-prefix%§ §%command-language-already-using%§");
			return;
		}
		player.setLanguage(lang);
		p.sendMessage("§%command-language-prefix%§ " + T.t(BukkitMain.getInstance(), lang, "command-language-success", new String[] { "%language%", lang.toString() }));
	}

	@Command(name = "preferences", aliases = { "prefs", "preferencias" })
	public void preferences(BukkitCommandArgs args) {

	}

}

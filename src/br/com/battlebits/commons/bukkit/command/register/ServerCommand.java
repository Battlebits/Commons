package br.com.battlebits.commons.bukkit.command.register;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.bukkit.command.BukkitCommandArgs;
import br.com.battlebits.commons.bukkit.listener.PlayerListener;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework;
import br.com.battlebits.commons.core.data.DataPlayer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.punish.Kick;
import br.com.battlebits.commons.core.translate.T;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by gustavo on 28/05/17.
 */
public class ServerCommand implements CommandClass {

    // /ahalert {player} {tps} {ping} {hacktype}
    @CommandFramework.Command(name="ahalert",groupToUse = Group.DONO)
    public void ahalert(BukkitCommandArgs args) {
        if(args.isPlayer())
            return;
        if(args.getArgs().length < 4)
            return;
        String playerName = args.getArgs()[0];
        Player target = Bukkit.getPlayer(playerName);
        if(target == null || !target.isOnline())
            return;
        double tps = Double.parseDouble(args.getArgs()[1]);
        int ping = Integer.parseInt(args.getArgs()[2]);
        StringBuilder hackType = new StringBuilder(args.getArgs()[3]);
        for(int i = 4; i < args.getArgs().length; i++) {
            hackType.append(" ").append(args.getArgs()[i]);
        }

        for (Player online : Bukkit.getOnlinePlayers()) {
            BattlePlayer pl = BattlebitsAPI.getAccountCommon().getBattlePlayer(online.getUniqueId());
            if (pl.hasGroupPermission(Group.TRIAL) && pl.getConfiguration().isAlertsEnabled()) {
                TextComponent message = new TextComponent(T.t(BukkitMain.getInstance(), pl.getLanguage(),
                        "anticheat-alert-prefix")
                        + " "
                        + T.t(BukkitMain.getInstance(), pl.getLanguage(), "anticheat-alert-player",
                        new String[]{"%player%", "%tps%", "%ping%", "%suspect%"},
                        new String[]{target.getName(), "" + tps, "" + ping, hackType.toString()}));
                message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/teleport " + target.getName()));
                message.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT,
                        new TextComponent[]{new TextComponent(
                                T.t(BukkitMain.getInstance(), pl.getLanguage(), "click-to-teleport"))}));
                online.spigot().sendMessage(message);
            }
        }
    }

    // /ahalert {player} {reason}
    @CommandFramework.Command(name="ahkick",groupToUse = Group.DONO)
    public void ahkick(BukkitCommandArgs args) {
        if(args.isPlayer())
            return;
        if(args.getArgs().length < 2)
            return;
        String playerName = args.getArgs()[0];
        Player target = Bukkit.getPlayer(playerName);
        if(target == null || !target.isOnline())
            return;
        BattlePlayer player = BattlePlayer.getPlayer(target.getUniqueId());
        if(player == null)
            return;
        StringBuilder reason = new StringBuilder(args.getArgs()[1]);
        for(int i = 2; i < args.getArgs().length; i++) {
            reason.append(" ").append(args.getArgs()[i]);
        }
        if(player.getPunishHistoric().shouldAutoban()) {
            if(PlayerListener.willAutoban(target))
                return;
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("AnticheatBanAlert");
            out.writeUTF(reason.toString());
            target.sendPluginMessage(BukkitMain.getInstance(), BattlebitsAPI.getBungeeChannel(), out.toByteArray());
            PlayerListener.autoban(target, 60, reason.toString());
            return;
        }
        player.getPunishHistoric().getAntihackKickHistory().add(new Kick(BattlebitsAPI.getServerId(), reason.toString()));
        DataPlayer.saveBattlePlayer(player, "punishHistoric");
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("AnticheatKickAlert");
        out.writeUTF(reason.toString());
        target.sendPluginMessage(BukkitMain.getInstance(), BattlebitsAPI.getBungeeChannel(), out.toByteArray());
        target.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD +  "ANTICHEAT " + ChatColor.WHITE + reason);
    }

    @CommandFramework.Command(name="autobanremove", aliases = {"abremove", "banrm"},groupToUse = Group.TRIAL, runAsync = true)
    public void autobanremove(BukkitCommandArgs args) {
        if(args.isPlayer())
            return;
        if(args.getArgs().length < 1)
            return;
        UUID uuid = BattlebitsAPI.getUUIDOf(args.getArgs()[0]);
        if(!PlayerListener.willAutoban(uuid)) {
            args.getSender().sendMessage("§%command-autoban-dont-have-ban%§");
            return;
        }
        PlayerListener.cancelAutoban(uuid);
        args.getSender().sendMessage("§%command-autoban-canceled%§");
    }

}

package br.com.battlebits.commons.bukkit.listener;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.admin.AdminMode;
import br.com.battlebits.commons.api.vanish.VanishAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.bukkit.event.PlayerMoveBlockEvent;
import br.com.battlebits.commons.bukkit.event.PlayerMoveUpdateEvent;
import br.com.battlebits.commons.bukkit.event.account.PlayerChangeLeagueEvent;
import br.com.battlebits.commons.bukkit.event.account.PlayerLanguageEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;
import br.com.battlebits.commons.bukkit.event.vanish.PlayerShowToPlayerEvent;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.data.DataServer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.punish.Kick;
import br.com.battlebits.commons.core.translate.T;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerListener implements Listener {

    private static HashMap<UUID, Kick> autoban = new HashMap<>();

    public static void autoban(Player player, int seconds, String reason) {
        autoban.put(player.getUniqueId(), new Kick(BattlebitsAPI.getServerId(), reason, System.currentTimeMillis() + (seconds * 1000)));
    }

    public static boolean willAutoban(Player player) {
        return willAutoban(player.getUniqueId());
    }

    public static boolean willAutoban(UUID uuid) {
        return autoban.containsKey(uuid);
    }

    public static void cancelAutoban(UUID uuid) {
        autoban.remove(uuid);
    }


    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPreProcessCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().toLowerCase().startsWith("/me ")) {
            event.setCancelled(true);
        }
        if (event.getMessage().split(" ")[0].contains(":")) {
            event.getPlayer().sendMessage("§%cant-type-two-dot-command%§");
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onWhitelist(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().toLowerCase().startsWith("/whitelist ")) {
            if (event.getPlayer().hasPermission("minecraft.command.whitelist") || event.getPlayer().hasPermission("bukkit.command.whitelist")) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        DataServer.setJoinEnabled(!Bukkit.hasWhitelist());
                    }
                }.runTaskLaterAsynchronously(BukkitMain.getInstance(), 2);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        if (BattlebitsAPI.isChristmas())
            new BukkitRunnable() {
                @Override
                public void run() {
                    event.getPlayer().sendMessage("§%merry-christmas%§");
                }
            }.runTaskLater(BukkitMain.getInstance(), 2);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoinMonitor(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        VanishAPI.getInstance().updateVanishToPlayer(player);
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (online.getUniqueId().equals(player.getUniqueId()))
                continue;
            PlayerShowToPlayerEvent eventCall = new PlayerShowToPlayerEvent(player, online);
            Bukkit.getPluginManager().callEvent(eventCall);
            if (eventCall.isCancelled()) {
                if (online.canSee(player))
                    online.hidePlayer(player);
            } else if (!online.canSee(player))
                online.showPlayer(player);
        }
    }

    @EventHandler
    public void onLanguage(PlayerLanguageEvent event) {
        Player p = event.getPlayer();
        Scoreboard board = p.getScoreboard();
        p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        p.setScoreboard(board);
        p.updateInventory();
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        if (event.getResult() == Result.KICK_WHITELIST) {
            BattlePlayer player = BattlebitsAPI.getAccountCommon().getBattlePlayer(event.getPlayer().getUniqueId());
            if (player == null) {
                event.disallow(Result.KICK_OTHER, ChatColor.RED + "ERROR");
            } else if (player.hasGroupPermission(Group.MODPLUS)) {
                event.allow();
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        locations.remove(event.getPlayer().getUniqueId());
        AdminMode.getInstance().removeAdmin(event.getPlayer());
        VanishAPI.getInstance().removeVanish(event.getPlayer());
        if (willAutoban(event.getPlayer())) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("AnticheatBan");
            Kick kick = autoban.get(event.getPlayer().getUniqueId());
            out.writeUTF(kick.getReason());
            event.getPlayer().sendPluginMessage(BukkitMain.getInstance(), BattlebitsAPI.getBungeeChannel(), out.toByteArray());
            autoban.remove(event.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void onPlayerChangeLeague(PlayerChangeLeagueEvent event) {
        if (event.getPlayer() != null && event.getNewLeague().ordinal() > event.getOldLeague().ordinal()) {
            HashMap<String, String> replaces = new HashMap<>();
            replaces.put("%league%", event.getNewLeague().toString());
            replaces.put("%symbol%", event.getNewLeague().getSymbol());
            event.getPlayer().sendMessage("§%league-prefix%§ " + T.t(BukkitMain.getInstance(), event.getBukkitPlayer().getLanguage(), "league-rank-level-up", replaces));
        } else if (event.getNewLeague().ordinal() < event.getOldLeague().ordinal()) {
            HashMap<String, String> replaces = new HashMap<>();
            replaces.put("%league%", event.getNewLeague().toString());
            replaces.put("%symbol%", event.getNewLeague().getSymbol());
            event.getPlayer().sendMessage("§%league-prefix%§ " + T.t(BukkitMain.getInstance(), event.getBukkitPlayer().getLanguage(), "league-rank-level-down", replaces));
        }
    }

    @EventHandler
    public void onUpdate(UpdateEvent event) {
        if (event.getType() != UpdateEvent.UpdateType.SECOND)
            return;
        for (Map.Entry<UUID, Kick> entry : new HashMap<>(autoban).entrySet()) {
            if (entry.getValue().getKickTime() < System.currentTimeMillis()) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("AnticheatBan");
                out.writeUTF(entry.getValue().getReason());
                Bukkit.getPlayer(entry.getKey()).sendPluginMessage(BukkitMain.getInstance(), BattlebitsAPI.getBungeeChannel(), out.toByteArray());
                autoban.remove(entry.getKey());
            }
        }
    }

    protected final Map<UUID, Location> locations = new HashMap<>();

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerMove(UpdateEvent event) {
        if (event.getType() != UpdateType.TICK)
            return;
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(locations.containsKey(p.getUniqueId())) {
                Location to = p.getLocation();
                Location from = locations.get(p.getUniqueId());
                if ((to.getX() != from.getX()) && (to.getY() != from.getY()) && (to.getZ() != from.getZ())) {
                    PlayerMoveUpdateEvent moveUpdateEvent = new PlayerMoveUpdateEvent(p, from, to);
                    Bukkit.getServer().getPluginManager().callEvent(moveUpdateEvent);
                    if(moveUpdateEvent.isCancelled())
                        p.teleport(from);
                }
                if ((to.getBlockX() != from.getBlockX()) && (to.getBlockY() != from.getBlockY()) && (to.getBlockZ() != from.getBlockZ())) {
                    PlayerMoveBlockEvent moveBlockEvent = new PlayerMoveBlockEvent(p, from, to);
                    Bukkit.getServer().getPluginManager().callEvent(moveBlockEvent);
                    if(moveBlockEvent.isCancelled())
                        p.teleport(from);
                }
            }
            locations.put(p.getUniqueId(), p.getLocation());
        }
    }

}
package br.com.battlebits.commons.bukkit.listener;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.vanish.VanishAPI;
import br.com.battlebits.commons.bukkit.BukkitMain;
import br.com.battlebits.commons.bukkit.account.BukkitPlayer;
import br.com.battlebits.commons.bukkit.event.account.PlayerChangeGroupEvent;
import br.com.battlebits.commons.bukkit.event.account.PlayerUpdateFieldEvent;
import br.com.battlebits.commons.bukkit.event.account.PlayerUpdatedFieldEvent;
import br.com.battlebits.commons.bukkit.party.BukkitParty;
import br.com.battlebits.commons.bukkit.protocol.ProtocolGetter;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.account.League;
import br.com.battlebits.commons.core.clan.Clan;
import br.com.battlebits.commons.core.data.DataClan;
import br.com.battlebits.commons.core.data.DataParty;
import br.com.battlebits.commons.core.data.DataPlayer;
import br.com.battlebits.commons.core.data.DataServer;
import br.com.battlebits.commons.core.party.Party;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.util.GeoIpUtils;
import br.com.battlebits.commons.util.GeoIpUtils.IpInfo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.net.InetAddress;
import java.sql.PreparedStatement;
import java.util.UUID;

public class AccountListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public synchronized void onAsync(AsyncPlayerPreLoginEvent event) {
        if (Bukkit.getPlayer(event.getUniqueId()) != null) {
            event.setLoginResult(Result.KICK_OTHER);
            event.setKickMessage("Already online");
            return;
        }
        String userName = event.getName();
        InetAddress ipAdress = event.getAddress();
        UUID uuid = event.getUniqueId();
        BattlebitsAPI.debug("CONNECTION > STARTING");
        try {
            IpInfo ipinfo = GeoIpUtils.getIpInfo(ipAdress.getHostAddress());
            BukkitPlayer player = DataPlayer.getRedisBukkitPlayer(uuid);
            if (player == null) {
                player = DataPlayer.createIfNotExistMongoBukkit(uuid, userName, ipAdress.getHostAddress(), ipinfo);
                DataPlayer.saveRedisPlayer(player);
                player.setCacheOnQuit(true);
                BattlebitsAPI.debug("CONNECTION > TRYING MONGO");
            } else {
                if (DataPlayer.checkCache(uuid))
                    player.setCacheOnQuit(true);
                BattlebitsAPI.debug("CONNECTION > REDIS FOUND");
            }

            player.connect(BattlebitsAPI.getServerId(), BattlebitsAPI.getServerType());
            player.setJoinData(userName, ipAdress.getHostAddress(), ipinfo);
            BattlebitsAPI.getAccountCommon().loadBattlePlayer(uuid, player);
            DataClan.loadClan(player);

            /*if (player.getClanUniqueId() != null) {
                try {
                    Clan clan = BattlebitsAPI.getClanCommon().getClan(player.getClanUniqueId());
                    if (clan == null) {
                        clan = DataClan.getRedisClan(player.getClanUniqueId());
                        if (clan == null) {
                            clan = DataClan.getMongoClan(player.getClanUniqueId());
                            if (clan != null) {
                                DataClan.saveRedisClan(clan);
                            }
                        } else {
                            if (DataClan.checkCache(player.getClanUniqueId()))
                                clan.setCacheOnQuit(true);
                        }
                        if (clan != null) {
                            BattlebitsAPI.getClanCommon().loadClan(clan);
                        }
                    }
                    if (clan != null) {
                        if (!clan.isParticipant(player))
                            player.setClanUniqueId(null);
                        if (player.getClanUniqueId() != null) {
                            clan.updatePlayer(player);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

			/* Party */
            Party party = BattlebitsAPI.getPartyCommon().getByOwner(uuid);
            if (party == null) {
                party = DataParty.getRedisParty(uuid, BukkitParty.class);
                if (party != null)
                    BattlebitsAPI.getPartyCommon().loadParty(party);
            }

            BattlebitsAPI.debug("ACCOUNT > CLOSE");
        } catch (Exception e) {
            event.setKickMessage(T.t(BukkitMain.getInstance(), BattlebitsAPI.getDefaultLanguage(), "account-load-failed"));
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onRemoveAccount(AsyncPlayerPreLoginEvent event) {
        if (BattlebitsAPI.getAccountCommon().getBattlePlayer(event.getUniqueId()) == null) {
            event.setLoginResult(Result.KICK_OTHER);
            event.setKickMessage(T.t(BukkitMain.getInstance(), BattlebitsAPI.getDefaultLanguage(), "account-not-load"));
        }
        if (event.getLoginResult() != Result.ALLOWED) {
            removePlayer(event.getUniqueId(), -1);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLogin(PlayerLoginEvent event) {
        if (event.getResult() != org.bukkit.event.player.PlayerLoginEvent.Result.ALLOWED)
            return;
        if (BattlePlayer.getPlayer(event.getPlayer().getUniqueId()) == null) {
            event.disallow(org.bukkit.event.player.PlayerLoginEvent.Result.KICK_OTHER, T.t(BukkitMain.getInstance(), BattlebitsAPI.getDefaultLanguage(), "account-not-load"));
            return;
        }
        new BukkitRunnable() {

            @Override
            public void run() {
                DataServer.joinPlayer(event.getPlayer().getUniqueId());
                int protocolVersion = ProtocolGetter.getVersion(event.getPlayer()).getId();
                try (PreparedStatement stmt = BattlebitsAPI.getCommonsMysql().prepareStatment("INSERT INTO `player_log`(`uuid`, `protocol`, `event`, `server`, `servertype`, `date`) VALUES (?, " + protocolVersion + ", ?, ?, ?, NOW());")) {
                    stmt.setString(1, event.getPlayer().getUniqueId().toString().replace("-",""));
                    stmt.setString(2, "JOIN");
                    stmt.setString(3, BattlebitsAPI.getServerId());
                    stmt.setString(4, BattlebitsAPI.getServerType().toString());
                    stmt.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(BukkitMain.getInstance());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();

		/* Party */
        Party party = BattlebitsAPI.getPartyCommon().getByOwner(uuid);
        if (party == null) {
            party = BattlebitsAPI.getPartyCommon().getParty(uuid);
            if (party != null)
                party.onMemberJoin(uuid);
        } else {
            party.onOwnerJoin();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeave(PlayerQuitEvent event) {
        int protocolVersion = ProtocolGetter.getVersion(event.getPlayer()).getId();
        Bukkit.getScheduler().runTaskAsynchronously(BukkitMain.getInstance(), () -> removePlayer(event.getPlayer().getUniqueId(), protocolVersion));
    }

    private void removePlayer(UUID uniqueId, int protocolVersion) {
        BukkitPlayer player = (BukkitPlayer) BattlePlayer.getPlayer(uniqueId);
        if (player == null)
            return;

        if (player.getClan() != null) {
            Clan clan = player.getClan();
            boolean removeClan = true;
            for (UUID uuid : clan.getParticipants().keySet()) {
                if (uuid.equals(player.getUniqueId()))
                    continue;
                Player p = Bukkit.getPlayer(uuid);
                if (p != null && p.isOnline()) {
                    removeClan = false;
                    break;
                }
            }
            if (removeClan) {
                if (clan.isCacheOnQuit())
                    DataClan.cacheRedisClan(clan.getUniqueId(), clan.getName());
                BattlebitsAPI.getClanCommon().unloadClan(player.getClanUniqueId());
            }
        }

        Party party = BattlebitsAPI.getPartyCommon().getByOwner(uniqueId);
        if (party == null) {
            party = BattlebitsAPI.getPartyCommon().getParty(uniqueId);
            if (party != null) {
                party.onMemberLeave(uniqueId);
                if (party.getOnlineCount() == 0)
                    BattlebitsAPI.getPartyCommon().removeParty(party);
            }
        } else {
            party.onOwnerLeave();
            if (party.getOnlineCount() == 0)
                BattlebitsAPI.getPartyCommon().removeParty(party);
        }

        if (player.isCacheOnQuit())
            DataPlayer.cacheRedisPlayer(uniqueId);
        DataServer.leavePlayer(player.getUniqueId());
        try (PreparedStatement stmt = BattlebitsAPI.getCommonsMysql().prepareStatment("INSERT INTO `player_log`(`uuid`, `protocol`, `event`, `server`, `servertype`, `date`) VALUES (?, " + protocolVersion + ", ?, ?, ?, NOW());")) {
            stmt.setString(1, player.getUniqueId().toString().replace("-",""));
            stmt.setString(2, "LEAVE");
            stmt.setString(3, BattlebitsAPI.getServerId());
            stmt.setString(4, BattlebitsAPI.getServerType().toString());
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BattlebitsAPI.getAccountCommon().unloadBattlePlayer(uniqueId);
    }

    @EventHandler
    public void onUpdateField(PlayerUpdateFieldEvent event) {
        BukkitPlayer player = event.getBukkitPlayer();
        switch (event.getField()){
            case "league":
                player.setLeague((League) event.getObject());
                event.setCancelled(true);
                break;
            default:
                break;
        }
    }

    @EventHandler
    public void onUpdate(PlayerUpdatedFieldEvent event) {
        BukkitPlayer player = event.getBukkitPlayer();
        switch (event.getField()) {
            case "clanUniqueId":
                if (event.getObject() != null)
                    BattlebitsAPI.getClanCommon().loadClan(DataClan.getClan(player.getClanUniqueId()));
                else if (player.getClanUniqueId() != null && player.getClan() != null) {
                    Clan clan = player.getClan();
                    boolean removeClan = true;
                    for (UUID uuid : clan.getParticipants().keySet()) {
                        if (uuid.equals(player.getUniqueId()))
                            continue;
                        Player p = Bukkit.getPlayer(uuid);
                        if (p != null && p.isOnline()) {
                            removeClan = false;
                            break;
                        }
                    }
                    if (removeClan) {
                        if (clan.isCacheOnQuit())
                            DataClan.cacheRedisClan(clan.getUniqueId(), clan.getName());
                        BattlebitsAPI.getClanCommon().unloadClan(player.getClanUniqueId());
                    }
                }
                break;
            case "groups":
            case "ranks":
                player.loadTags();
                player.setTag(player.getDefaultTag());
                VanishAPI.getInstance().updateVanishToPlayer(event.getPlayer());
                Bukkit.getPluginManager().callEvent(new PlayerChangeGroupEvent(event.getPlayer(), player, player.getServerGroup()));
                break;
            default:
                break;
        }
    }
}

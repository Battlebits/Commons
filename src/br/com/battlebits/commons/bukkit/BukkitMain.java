package br.com.battlebits.commons.bukkit;

import br.com.battlebits.commons.api.cooldown.CooldownAPI;
import br.com.battlebits.commons.bukkit.injector.PacketLimiterInjector;
import br.com.battlebits.commons.core.backend.sql.MySQLBackend;
import br.com.battlebits.commons.core.data.DataServer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.bossbar.BossBarAPI;
import br.com.battlebits.commons.api.item.ActionItemListener;
import br.com.battlebits.commons.api.menu.MenuListener;
import br.com.battlebits.commons.bukkit.command.BukkitCommandFramework;
import br.com.battlebits.commons.bukkit.generator.VoidGenerator;
import br.com.battlebits.commons.bukkit.injector.ActionItemInjector;
import br.com.battlebits.commons.bukkit.injector.ServerInfoInjector;
import br.com.battlebits.commons.bukkit.injector.TranslationInjector;
import br.com.battlebits.commons.bukkit.listener.AccountListener;
import br.com.battlebits.commons.bukkit.listener.AntiAFK;
import br.com.battlebits.commons.bukkit.listener.ChatListener;
import br.com.battlebits.commons.bukkit.listener.PlayerNBTListener;
import br.com.battlebits.commons.bukkit.listener.PlayerListener;
import br.com.battlebits.commons.bukkit.listener.ScoreboardListener;
import br.com.battlebits.commons.bukkit.messenger.BungeeCordMessenger;
import br.com.battlebits.commons.bukkit.permission.PermissionManager;
import br.com.battlebits.commons.bukkit.protocol.ProtocolGetter;
import br.com.battlebits.commons.bukkit.redis.BukkitPubSubHandler;
import br.com.battlebits.commons.bukkit.scheduler.UpdateScheduler;
import br.com.battlebits.commons.bukkit.scoreboard.tagmanager.TagManager;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.backend.mongodb.MongoBackend;
import br.com.battlebits.commons.core.backend.redis.PubSubListener;
import br.com.battlebits.commons.core.backend.redis.RedisBackend;
import br.com.battlebits.commons.core.command.CommandLoader;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.updater.AutoUpdater;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.util.*;

@Getter
public class BukkitMain extends JavaPlugin {
    private ProtocolManager procotolManager;
    private PermissionManager permissionManager;
    private TagManager tagManager;
    @Getter
    private static BukkitMain instance;
    private boolean oldTag = false;
    @Setter
    private boolean tagControl = true;
    private PubSubListener pubSubListener;
    @Setter
    private boolean antiAfkEnabled = true;

    private boolean removePlayerDat = true;

    @Setter
    @Getter
    private boolean bossbarAddress = false;

    private String mongoHostname;
    private String mongoDatabase;
    private String mongoUsername;
    private String mongoPassword;
    private int mongoPort = 27017;

    private String mysqlHostname;
    private String mysqlDatabase;
    private String mysqlUsername;
    private String mysqlPassword;
    private int mysqlPort = 3306;

    private String redisHostname;
    private String redisPassword;
    private int redisPort = 6379;

    @Override
    public void onLoad() {
        instance = this;
        new AutoUpdater(this, "vAPS4jf?&R_}E25T").run();
        Plugin plugin = getServer().getPluginManager().getPlugin("ViaVersion");
        if (plugin != null)
            new AutoUpdater(plugin, "Yw7~=#/7Uw(L:;QG").run();
        plugin = getServer().getPluginManager().getPlugin("ProtocolSupport");
        if (plugin != null)
            new AutoUpdater(plugin, "Y285jZEB<-CPs{8x").run();
        plugin = getServer().getPluginManager().getPlugin("ProtocolLib");
        if (plugin != null)
            new AutoUpdater(plugin, "LV3SMrVwM_-BR~q7").run();
        plugin = getServer().getPluginManager().getPlugin("ProtocolSupportLegacyHologram");
        if (plugin != null)
            new AutoUpdater(plugin, "r'E9q(@P?2dVc4Ng").run();
        procotolManager = ProtocolLibrary.getProtocolManager();
        new ActionItemInjector().inject(this);
        new TranslationInjector().inject(this);
        new ServerInfoInjector().inject(this);
    }

    @Override
    public void onEnable() {
        loadConfiguration();
        new PacketLimiterInjector().inject(this);
        BattlebitsAPI.setPlatform(new BukkitPlatform());
        BattlebitsAPI.setLogger(getLogger());
        try {
            MongoBackend mongoBackend = new MongoBackend(mongoHostname, mongoDatabase, mongoUsername, mongoPassword, mongoPort);
            RedisBackend redisBackend = new RedisBackend(redisHostname, redisPassword, redisPort);
            MySQLBackend mysqlBackend = new MySQLBackend(mysqlHostname, mysqlDatabase, mysqlUsername, mysqlPassword,
                    mysqlPort);
            mongoBackend.startConnection();
            redisBackend.startConnection();
            BattlebitsAPI.setCommonsMongo(mongoBackend);
            BattlebitsAPI.setCommonsMysql(mysqlBackend);
            BattlebitsAPI.setCommonsRedis(redisBackend);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ProtocolGetter.foundDependencies();
        BattlebitsAPI.setServerAddress(Bukkit.getIp() + ":" + Bukkit.getPort());
        BattlebitsAPI.setServerId(DataServer.getServerId(BattlebitsAPI.getServerAddress()));
        BattlebitsAPI.setServerType(DataServer.getServerType(BattlebitsAPI.getServerAddress()));
        try (PreparedStatement stmt = BattlebitsAPI.getCommonsMysql().prepareStatment("INSERT INTO `server_log`(`server`, `servertype`, `event`, `date`) VALUES (?, ?, ?, NOW());")) {
            stmt.setString(1, BattlebitsAPI.getServerId());
            stmt.setString(2, BattlebitsAPI.getServerType().toString());
            stmt.setString(3, "START");
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BattlebitsAPI.getLogger().info("Battlebits Server carregado. ServerId: " + BattlebitsAPI.getServerId());
        BattlebitsAPI.getLogger().info("Server type: " + BattlebitsAPI.getServerType());
        DataServer.newServer(Bukkit.getMaxPlayers());
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, BattlebitsAPI.getBungeeChannel());
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeCordMessenger());
        Translate translate = new Translate(BattlebitsAPI.TRANSLATION_ID, BattlebitsAPI.getCommonsMongo());
        translate.loadTranslations();
        T.loadTranslate(this, translate);
        BattlebitsAPI.setTranslate(translate);
        registerListeners();
        registerCommonManagement();
        enableCommonManagement();
        getServer().getScheduler().runTaskTimer(this, new UpdateScheduler(), 1, 1);
        getServer().getScheduler().runTaskLater(this, () -> unregisterCommands("pl", "plugins", "icanhasbukkit", "ver", "version", "?", "help", "viaversion", "viaver", "vvbukkit", "protocolsupport", "ps", "holograms", "hd", "holo", "hologram"), 2L);
        getServer().getScheduler().runTaskAsynchronously(this, pubSubListener = new PubSubListener(new BukkitPubSubHandler(), "account-field", "clan-field", "party-field", "party-action"));
        try {
            new CommandLoader(new BukkitCommandFramework(this)).loadCommandsFromPackage(getFile(), "br.com.battlebits.commons.bukkit.command.register");
        } catch (Exception e) {
            BattlebitsAPI.getLogger().warning("Erro ao carregar o commandFramework!");
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        DataServer.stopServer();
        BattlebitsAPI.getCommonsMongo().closeConnection();
        BattlebitsAPI.getCommonsRedis().closeConnection();
        try {
            try (PreparedStatement stmt = BattlebitsAPI.getCommonsMysql().prepareStatment("INSERT INTO `server_log`(`server`, `servertype`, `event`, `date`) VALUES (?, ?, ?, NOW());")) {
                stmt.setString(1, BattlebitsAPI.getServerId());
                stmt.setString(2, BattlebitsAPI.getServerType().toString());
                stmt.setString(3, "STOP");
                stmt.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BattlebitsAPI.getCommonsMysql().closeConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadConfiguration() {
        saveDefaultConfig();
        removePlayerDat = getConfig().getBoolean("remove-player-dat", false);

        mongoHostname = getConfig().getString("mongo.hostname", "localhost");
        mongoPort = getConfig().getInt("mongo.port", 27017);
        mongoDatabase = getConfig().getString("mongo.database", "");
        mongoUsername = getConfig().getString("mongo.username", "");
        mongoPassword = getConfig().getString("mongo.password", "");

        mysqlHostname = getConfig().getString("mysql.hostname", "localhost");
        mysqlPort = getConfig().getInt("mysql.port", 3306);
        mysqlDatabase = getConfig().getString("mysql.database", "");
        mysqlUsername = getConfig().getString("mysql.username", "");
        mysqlPassword = getConfig().getString("mysql.password", "");

        redisHostname = getConfig().getString("redis.hostname", "localhost");
        redisPassword = getConfig().getString("redis.password", "");
        redisPort = getConfig().getInt("redis.port", 6379);
    }

    private void registerListeners() {
        PluginManager pm = getServer().getPluginManager();

        if (isAntiAfkEnabled())
            pm.registerEvents(new AntiAFK(), this);

        pm.registerEvents(new AccountListener(), this);
        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(new PlayerNBTListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new ScoreboardListener(), this);

        // APIs
        pm.registerEvents(new ActionItemListener(), this);
        pm.registerEvents(new BossBarAPI(), this);
        pm.registerEvents(new CooldownAPI(), this);
        pm.registerEvents(new MenuListener(), this);
    }

    public static void broadcast(String text) {
        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(text));
    }

    public static void broadcast(String id, String[]... replaces) {
        broadcast(getInstance(), id, replaces);
    }

    public static void broadcast(String id, Map<String, String> replaces) {
        broadcast(getInstance(), id, replaces);
    }

    public static void broadcast(Plugin plugin, String id, String[]... replaces) {
        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(T.t(plugin, BattlePlayer.getLanguage(p.getUniqueId()), id, replaces)));
    }

    public static void broadcast(Plugin plugin, String id, Map<String, String> replaces) {
        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(T.t(plugin, BattlePlayer.getLanguage(p.getUniqueId()), id, replaces)));
    }

    private void registerCommonManagement() {
        permissionManager = new PermissionManager(this);
        tagManager = new TagManager(this);
    }

    private void enableCommonManagement() {
        permissionManager.onEnable();
        tagManager.onEnable();
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new VoidGenerator();
    }

    @SuppressWarnings("unchecked")
    public void unregisterCommands(String... commands) {
        try {
            Field f1 = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            f1.setAccessible(true);
            CommandMap commandMap = (CommandMap) f1.get(Bukkit.getServer());
            Field f2 = commandMap.getClass().getDeclaredField("knownCommands");
            f2.setAccessible(true);
            HashMap<String, Command> knownCommands = (HashMap<String, Command>) f2.get(commandMap);
            for (String command : commands) {
                if (knownCommands.containsKey(command)) {
                    knownCommands.remove(command);
                    List<String> aliases = new ArrayList<>();
                    for (String key : knownCommands.keySet()) {
                        if (!key.contains(":")) continue;
                        String substr = key.substring(key.indexOf(":")+1);
                        if (substr.equalsIgnoreCase(command)) {
                            aliases.add(key);
                        }
                    }
                    for (String alias : aliases) {
                        knownCommands.remove(alias);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

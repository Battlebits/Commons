package br.com.battlebits.commons;

import java.time.LocalDate;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.battlebits.commons.core.BattlePlatform;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import br.com.battlebits.commons.core.account.AccountCommon;
import br.com.battlebits.commons.core.account.AccountVersion;
import br.com.battlebits.commons.core.account.Tournament;
import br.com.battlebits.commons.core.backend.mongodb.MongoBackend;
import br.com.battlebits.commons.core.backend.redis.RedisBackend;
import br.com.battlebits.commons.core.backend.sql.MySQLBackend;
import br.com.battlebits.commons.core.clan.ClanCommon;
import br.com.battlebits.commons.core.party.PartyCommon;
import br.com.battlebits.commons.core.server.ServerType;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.mojang.NameFetcher;
import br.com.battlebits.commons.util.mojang.UUIDFetcher;
import br.com.battlebits.commons.util.timezone.TimeZone;
import lombok.Getter;
import lombok.Setter;

public class BattlebitsAPI {

    @Getter
    @Setter
    private static MySQLBackend commonsMysql;
    @Getter
    @Setter
    private static MongoBackend commonsMongo;
    @Getter
    @Setter
    private static RedisBackend commonsRedis;

    @Getter
    private static ClanCommon clanCommon = new ClanCommon();
    @Getter
    private static PartyCommon partyCommon = new PartyCommon();
    @Getter
    private static AccountCommon accountCommon = new AccountCommon();
    @Getter
    private static AccountVersion defaultAccountVersion = AccountVersion.SEASON_4;
    @Getter
    private static Tournament tournament = null;
    private static UUIDFetcher uuidFetcher = new UUIDFetcher();
    private static NameFetcher nameFetcher = new NameFetcher();

    public final static long MULTIPLIER_DURATION = 60000 * 60;
    public final static String TRANSLATION_ID = "commons";
    public final static String FORUM_WEBSITE = "forum.battlebits.net";
    public final static String WEBSITE = "www.battlebits.net";
    public final static String SERVER_ADDRESS = "battlebits.net";
    public final static String STORE = "loja.battlebits.net";
    public final static String ADMIN_EMAIL = "admin@battlebits.net";
    public final static String TWITTER = "@BattlebitsMC";
    public static TimeZone DEFAULT_TIME_ZONE = TimeZone.GMT0;
    @Getter
    public static Language defaultLanguage = Language.PORTUGUESE;

    @Getter
    @Setter
    public static Translate translate;

    @Setter
    @Getter
    private static BattlePlatform platform;

    @Getter
    @Setter
    private static Logger logger;

    @Getter
    @Setter
    private static String serverId;

    @Getter
    @Setter
    private static ServerType serverType;

    @Getter
    @Setter
    private static String serverAddress;

    protected static boolean debugMode = false;

    @Getter
    private static final Gson gson = new Gson();
    @Getter
    private static final Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
    @Getter
    private static final JsonParser parser = new JsonParser();

    public static String getBungeeChannel() {
        return "yCommon";
    }

    public static boolean isChristmas() {
        LocalDate now = LocalDate.now();
        return now.getMonthValue() == 12 && now.getDayOfMonth() == 25;
    }

    public static boolean isChildrensDay() {
        LocalDate now = LocalDate.now();
        return now.getMonthValue() == 10 && now.getDayOfMonth() == 12;
    }

    public static boolean isHalloween() {
        LocalDate now = LocalDate.now();
        return now.getMonthValue() == 10 && now.getDayOfMonth() == 31;
    }

    public static boolean isNewYear() {
        LocalDate now = LocalDate.now();
        return now.getMonthValue() == 1 && now.getDayOfMonth() == 1;
    }

    public static void debug(String debugStr) {
        if (debugMode) {
            getLogger().log(Level.INFO, debugStr);
        }
    }

    public static UUID getUUIDOf(String name) {
        return uuidFetcher.getUUID(name);
    }

    public static String getNameOf(UUID uuid) {
        return nameFetcher.getName(uuid);
    }
}
